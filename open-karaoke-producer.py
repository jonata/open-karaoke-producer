#!/usr/bin/python3

import os, sys, threading
from PyQt5.QtWidgets import QApplication, QWidget, QScrollArea, QPushButton, QDesktopWidget, QLineEdit, QLabel
from PyQt5.QtGui import QIcon, QPainter, QColor, QPen, QFont, QPolygonF
from PyQt5.QtCore import Qt, QRect, QPointF, QTimer, QPropertyAnimation, QEasingCurve,QSize

from modules import file_io
from modules import waveform

import sounddevice
import numpy

path_okp = os.path.abspath(os.path.dirname(sys.argv[0]))
path_okp_graphics = os.path.join(path_okp, 'graphics')
path_home = os.path.expanduser("~")

class openkaraokeproducer(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Open Karaoke Producer')
        self.setWindowIcon(QIcon(os.path.join(path_okp_graphics, 'openkaraokeproducer.png')))

        self.lyrics_metadata = False
        self.lyrics_notes = False
        self.txt_file = False

        self.music_length = 0.01
        self.music_waveform = False
        self.music_tones = {}
        self.recording_note = False
        self.music_average = {}
        self.music_waveformsize = 2
        self.update_accuracy = 80

        self.selected_note = False
        self.now_previewing_note = False

        self.mediaplayer_zoom = 100.0
        self.mediaplayer_viewnotes = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11]
        self.mediaplayer_opacity = .1
        self.mediaplayer_current_position = 0.0
        self.mediaplayer_is_playing = False
        self.mediaplayer_view_mode = False

        self.list_of_tonenotes = {
                            'G#7' : 44 ,   'G7' : 43 ,    'F#7' : 42 ,   'F7' : 41 ,    'E7' : 40 ,    'D#7' : 39 ,   'D7' : 38 ,    'C#7' : 37 ,   'C7' : 36 ,    'B7' : 35 ,    'A#7' : 34 ,  'A7' : 33 ,
                            'G#6' : 32 ,   'G6' : 31 ,    'F#6' : 30 ,   'F6' : 29 ,    'E6' : 28 ,    'D#6' : 27 ,   'D6' : 26 ,    'C#6' : 25 ,   'C6' : 24 ,    'B6' : 23 ,    'A#6' : 22 ,  'A6' : 21 ,
                            'G#5' : 20 ,   'G5' : 19 ,    'F#5' : 18 ,   'F5' : 17 ,    'E5' : 16 ,    'D#5' : 15 ,   'D5' : 14 ,    'C#5' : 13 ,   'C5' : 12 ,    'B5' : 11 ,    'A#5' : 10 ,  'A5' : 9 ,
                            'G#4' : 8 ,    'G4' : 7 ,     'F#4' : 6 ,    'F4' : 5 ,     'E4' : 4 ,     'D#4' : 3 ,    'D4' : 2 ,     'C#4' : 1 ,    'C4' : 0 ,     'B4' : -1 ,    'A#4' : -2 ,  'A4' : -3 ,
                            'G#3' : -4 ,   'G3' : -5 ,    'F#3' : -6 ,   'F3' : -7 ,    'E3' : -8 ,    'D#3' : -9 ,   'D3' : -10 ,   'C#3' : -11 ,  'C3' : -12 ,   'B3' : -13 ,   'A#3' : -14 , 'A3' : -15 ,
                            'G#2' : -16 ,  'G2' : -17 ,   'F#2' : -18 ,  'F2' : -19 ,   'E2' : -20 ,   'D#2' : -21 ,  'D2' : -22 ,   'C#2' : -23 ,  'C2' : -24 ,   'B2' : -25 ,   'A#2' : -26 , 'A2' : -27 ,
                            'G#1' : -28 ,  'G1' : -29 ,   'F#1' : -30 ,  'F1' : -31 ,   'E1' : -32 ,   'D#1' : -33 ,  'D1' : -34 ,   'C#1' : -35 ,  'C1' : -36 ,   'B1' : -37 ,   'A#1' : -38 , 'A1' : -39
                            }
        self.list_of_tones = {v: k for k, v in self.list_of_tonenotes.items()}

        class lyrics_timeline(QWidget):
            def paintEvent(widget, paintEvent):
                painter = QPainter(widget)

                scroll_position = self.lyrics_timeline_scroll.horizontalScrollBar().value()
                scroll_width = self.width()

                painter.setRenderHint(QPainter.Antialiasing)
                if self.lyrics_metadata:
                    if self.mediaplayer_view_mode and self.music_waveform:
                        if self.mediaplayer_view_mode in ['waveform', 'verticalform']:
                            if len([*self.music_waveform]) > 1:
                                if self.mediaplayer_zoom in [*self.music_waveform]:
                                    waveform = self.music_waveform[self.mediaplayer_zoom]
                                else:
                                    for zoom in sorted([*self.music_waveform]):
                                        if zoom > self.mediaplayer_zoom:
                                            break
                                        else:
                                            last_zoom = zoom
                                    waveform = self.music_waveform[last_zoom]

                                painter.setOpacity(self.mediaplayer_opacity)
                                painter.setPen(QPen(QColor.fromRgb(21,52,80,255), 1, Qt.SolidLine))
                                painter.setBrush(QColor.fromRgb(21,52,80,alpha=200))

                                x_factor = 1

                                if not self.mediaplayer_zoom in self.music_waveform.keys():
                                    x_factor = self.mediaplayer_zoom/last_zoom
                                    scroll_position /= x_factor
                                    scroll_width /= x_factor

                                if self.mediaplayer_view_mode == 'waveform':
                                    x_position = scroll_position
                                    polygon = QPolygonF()

                                    for point in waveform[0][int(scroll_position):int(scroll_position+scroll_width)]:
                                        polygon.append(QPointF(x_position, (widget.height()*.5) + (point*(self.music_waveformsize*100))))
                                        x_position += x_factor

                                    for point in reversed(waveform[1][int(scroll_position):int(scroll_position+scroll_width)]):
                                        polygon.append(QPointF(x_position, (widget.height()*.5) + (point*(self.music_waveformsize*100))))
                                        x_position -= x_factor

                                    painter.drawPolygon(polygon)
                                elif self.mediaplayer_view_mode == 'verticalform':
                                    polygon1 = QPolygonF()
                                    polygon2 = QPolygonF()

                                    x_position = scroll_position
                                    polygon1.append(QPointF(0, widget.height()))
                                    for point in waveform[0][int(scroll_position):int(scroll_position+scroll_width)]:
                                        polygon1.append(QPointF(x_position, widget.height() + (-1.0*(point*(self.music_waveformsize*200)))))
                                        x_position += x_factor
                                    polygon1.append(QPointF(x_position, widget.height()))

                                    x_position = scroll_position
                                    polygon2.append(QPointF(0, widget.height()))
                                    for point in waveform[1][int(scroll_position):int(scroll_position+scroll_width)]:
                                        polygon2.append(QPointF(x_position, widget.height() + (point*(self.music_waveformsize*200))))
                                        x_position += x_factor
                                    polygon2.append(QPointF(x_position, widget.height()))

                                    painter.drawPolygon(polygon1)
                                    painter.drawPolygon(polygon2)
                                '''if waveform[2]:
                                    painter.setPen(QPen(QColor.fromRgb(0,200,240,50), 5, Qt.SolidLine))
                                    x_position = scroll_position
                                    polygon = QPolygonF()
                                    last_point = 0.0
                                    for point in waveform[2][int(scroll_position):int(scroll_position+scroll_width)]:
                                        polygon.append(QPointF(x_position, widget.height() - (point*(widget.height()*.0008))))
                                        x_position += x_factor

                                    painter.drawPolyline(polygon)'''
                        elif self.mediaplayer_view_mode == 'histogram':
                            if self.recording_note and self.recording_note in self.list_of_tonenotes and self.list_of_tonenotes[self.recording_note] in self.mediaplayer_viewnotes:
                                note_height = widget.height()/len(self.mediaplayer_viewnotes)
                                pitch_in_widget = self.mediaplayer_viewnotes.index(self.list_of_tonenotes[self.recording_note])
                                note_ref_rect = QRect(
                                                        scroll_position,
                                                        pitch_in_widget*note_height,
                                                        self.width(),
                                                        note_height
                                                    )
                                painter.setPen(Qt.NoPen)
                                painter.setBrush(QColor.fromRgb(255,255,150,alpha=100))
                                painter.drawRect(note_ref_rect)
                        elif self.mediaplayer_view_mode == 'notes':
                            note_height = widget.height()/len(self.mediaplayer_viewnotes)
                            note_size = ((widget.width() / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])
                            notes_number = 1

                            painter.setPen(Qt.NoPen)
                            for square in sorted([*self.music_tones]):
                                if self.music_tones[square] in self.list_of_tonenotes and self.list_of_tonenotes[self.music_tones[square]] in self.mediaplayer_viewnotes:
                                    pitch_in_widget = self.mediaplayer_viewnotes.index(self.list_of_tonenotes[self.music_tones[square]])
                                    note_ref_rect = QRect(
                                                            (square/44.100) * ( widget.width() / (self.music_length * 1000)),
                                                            pitch_in_widget*note_height,
                                                            note_size,
                                                            note_height
                                                        )
                                    painter.setBrush(QColor.fromRgb(200,200,80,alpha=50))
                                    painter.drawRect(note_ref_rect)
                                    note_ref_rect = QRect(
                                                            (square/44.100) * ( widget.width() / (self.music_length * 1000)) + 5,
                                                            pitch_in_widget*note_height,
                                                            note_size - 5,
                                                            note_height
                                                        )
                                    painter.setPen(QColor.fromRgb(255,255,255,alpha=150))
                                    painter.drawText(note_ref_rect,Qt.AlignLeft | Qt.AlignVCenter,self.music_tones[square])

                    painter.setOpacity(1)

                    note_size = ((widget.width() / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])*.25 #                   (( / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])*.5
                    start_in = self.lyrics_metadata['gap'] * ( widget.width() / (self.music_length * 1000))

                    painter.setPen(QPen(QColor.fromRgb(0,52,80,255), .2, Qt.SolidLine))

                    if self.lyrics_metadata['bpm']:
                        pixel = start_in
                        while pixel < widget.width():
                            painter.drawLine(pixel, 0, pixel, widget.height())
                            pixel += note_size

                    painter.setPen(QPen(QColor.fromRgb(255,0,0,200), 5, Qt.SolidLine))
                    painter.drawLine(self.mediaplayer_current_position * widget.width(), 0, self.mediaplayer_current_position * widget.width(), widget.height())

                    note_height = widget.height()/len(self.mediaplayer_viewnotes)
                    notes_number = 0

                    while notes_number < len(self.mediaplayer_viewnotes):
                        painter.setPen(QPen(QColor.fromRgb(100,100,100,150), .2, Qt.SolidLine))
                        painter.drawLine(scroll_position, note_height*notes_number, widget.width(), note_height*notes_number)
                        note_ref_rect = QRect(scroll_position + 10, note_height*notes_number, 100, note_height)
                        if self.now_previewing_note and self.now_previewing_note == self.list_of_tones[self.mediaplayer_viewnotes[notes_number]]:
                            painter.setPen(QPen(QColor.fromRgb(255,0,0), .2, Qt.SolidLine))
                        painter.drawText(note_ref_rect,Qt.AlignLeft | Qt.AlignVCenter,self.list_of_tones[self.mediaplayer_viewnotes[notes_number]])
                        notes_number += 1

                if self.lyrics_notes:
                    painter.setPen(QPen(QColor.fromRgb(0,52,80,255), 1, Qt.SolidLine))
                    painter.setBrush(QColor.fromRgb(21,52,80,alpha=200))

                    note_size = ((widget.width() / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])*.25 #                   (( / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])*.5
                    start_in = self.lyrics_metadata['gap'] * ( widget.width() / (self.music_length * 1000))
                    note_height = widget.height()/len(self.mediaplayer_viewnotes)

                    for note in self.lyrics_notes:
                        if len(note) > 2 and (note[2] in self.mediaplayer_viewnotes or note[-1] in ['-']):
                            if self.selected_note == note:
                                painter.setPen(QColor.fromRgb(0,0,0,alpha=255))
                            else:
                                painter.setPen(QColor.fromRgb(255,255,255,alpha=255))

                            if note[-1] in [':', '*', 'F']:
                                pitch_in_widget = self.mediaplayer_viewnotes.index(note[2])
                                note_rect = QRect(  start_in + (int(note[0]) * note_size) ,
                                                    pitch_in_widget*note_height,
                                                    int(note[1]) * note_size,
                                                    note_height
                                                )
                                painter.setBrush(QColor.fromRgb(21,52,80,alpha=200))
                                text = note[3]
                            elif note[-1] in ['-']:
                                note_rect = QRect(  start_in + (int(note[0]) * note_size) ,
                                                    (widget.height()*.5),
                                                    int(note[1]) * note_size,
                                                    note_height
                                                )
                                painter.setBrush(QColor.fromRgb(200,200,80,alpha=200))
                                text = '➥'
                            painter.drawRoundedRect(note_rect,3.0,3.0,Qt.AbsoluteSize)
                            painter.drawText(note_rect,Qt.AlignCenter,text)

                    painter.setOpacity(1)

                painter.end()


            def mousePressEvent(widget, event):
                note_size = ((widget.width() / (self.music_length/60.0)) / self.lyrics_metadata['bpm'])*.25
                start_in = self.lyrics_metadata['gap'] * ( widget.width() / (self.music_length * 1000))
                note_height = widget.height()/len(self.mediaplayer_viewnotes)

                self.selected_note = False
                for note in self.lyrics_notes:
                    length = 1
                    pitch = pitch = int(len(self.mediaplayer_viewnotes)/2)
                    if (note[-1] in ['-'] and len(note) > 2) or note[-1] in [':', '*', 'F']:
                        length = note[1]
                        if note[-1] in [':', '*', 'F'] and note[2] in self.mediaplayer_viewnotes:
                            pitch = self.mediaplayer_viewnotes.index(note[2])

                    if (int(pitch)*note_height) < event.pos().y() < (int(pitch)*note_height) + note_height and start_in + (int(note[0]) * note_size) <  event.pos().x() < start_in + (int(note[0]) * note_size) + (int(length) * note_size):
                        self.selected_note = note
                        self.player_controls.update_edit_sylable(self)
                        break

                self.player_controls.stop(self)
                if not self.selected_note:
                    self.mediaplayer_current_position = float(event.pos().x() / widget.width())
                else:
                    note_start = int(self.lyrics_metadata['gap'] * ( len(self.music_waveform['full']) / (self.music_length * 1000))) + int(((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[0]))
                    self.mediaplayer_current_position = float(note_start / len(self.music_waveform['full']))
                    if not self.selected_note[-1] in ['-']:
                        self.player_controls.play(self)
                    #self.mediaplayer_current_position = float(event.pos().x() / widget.width())# * len(self.music_waveform['full']))
                self.player_controls.update_player_controls_state(self)

            def mouseReleaseEvent(widget, event):

                widget.update()

            def mouseMoveEvent(widget, event):
                None#print('mouse moved ' + str(event.pos().x()) + ' x ' + str(event.pos().y()))

            def mouseDoubleClickEvent(widget, event):
                self.player_controls.new_note(self)
                widget.update()

        self.lyrics_timeline = lyrics_timeline(self)
        self.lyrics_timeline.setMouseTracking(True)

        class lyrics_timeline_scroll(QScrollArea):
            def enterEvent(widget, event):
                if self.music_waveform:
                    self.zoom_widgets.setVisible(True)
            def leaveEvent(widget, event):
                if self.music_waveform:
                    self.zoom_widgets.setVisible(False)

        self.lyrics_timeline_scroll = lyrics_timeline_scroll(parent=self)
        self.lyrics_timeline_scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.lyrics_timeline_scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.lyrics_timeline_scroll.setWidget(self.lyrics_timeline)
        #self.lyrics_timeline_scroll.horizontalScrollBar().valueChanged.connect(lambda:lyrics_timeline_scroll_updated(self))

        self.zoom_widgets = QWidget(parent=self.lyrics_timeline_scroll)
        self.zoom_widgets.setVisible(False)

        self.viewnotesin_button = QPushButton(parent=self.zoom_widgets)
        self.viewnotesin_button.setIconSize(QSize(14,8))
        self.viewnotesin_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_up.png')))
        self.viewnotesin_button.setStyleSheet('  QPushButton { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
        self.viewnotesin_button.clicked.connect(lambda:viewnotesin_button_clicked(self))

        self.viewnotesout_button = QPushButton(parent=self.zoom_widgets)
        self.viewnotesout_button.setIconSize(QSize(14,8))
        self.viewnotesout_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_down.png')))
        self.viewnotesout_button.setStyleSheet('  QPushButton { padding-top:1px; color:white; border-left: 5px; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:hover:pressed { border-left: 5px; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:disabled { border-left: 5px; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                 QPushButton:hover { border-left: 5px; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
        self.viewnotesout_button.clicked.connect(lambda:viewnotesout_button_clicked(self))

        self.zoomin_button = QPushButton(parent=self.zoom_widgets)
        self.zoomin_button.setIconSize(QSize(16,17))
        self.zoomin_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_zoomin.png')))
        self.zoomin_button.setStyleSheet('  QPushButton { color:white; border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                            QPushButton:checked { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                            QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                            QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                            QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
        self.zoomin_button.clicked.connect(lambda:zoomin_button_clicked(self))

        self.zoomout_button = QPushButton(parent=self.zoom_widgets)
        self.zoomout_button.setIconSize(QSize(16,17))
        self.zoomout_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_zoomout.png')))
        self.zoomout_button.setStyleSheet('  QPushButton { color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                             QPushButton:checked { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                             QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                             QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                             QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
        self.zoomout_button.clicked.connect(lambda:zoomout_button_clicked(self))

        self.start_screen = QLabel(parent=self)

        self.start_screen_background = QLabel(parent=self.start_screen)
        self.start_screen_background.setStyleSheet('QLabel { border-left: 80px; border-top:80px; border-image: url("' + os.path.join(path_okp_graphics, 'start_screen_background.png').replace('\\', '/') + '") 1 1 0 1 stretch stretch; \
                                                    image: url("' + os.path.join(path_okp_graphics, 'start_screen_logo.png').replace('\\', '/') + '"); qproperty-alignment: "AlignCenter";}')

        self.start_screen_right = QLabel(parent=self.start_screen)
        self.start_screen_right.setStyleSheet('QLabel { border-top:80px; border-image: url("' + os.path.join(path_okp_graphics, 'start_screen_background.png').replace('\\', '/') + '") 1 0 0 49 stretch stretch; }')

        self.start_screen_open_text = QLabel('Open a karaoke txt file or a mp3/oga file to start.', parent=self.start_screen)
        #self.start_screen_open_text.setWordWrap(True)
        self.start_screen_open_text.setStyleSheet('QLabel { font-size:12px; color:gray; qproperty-alignment: "AlignVCenter | AlignLeft"; qproperty-wordWrap: true}')

        self.start_screen_open_button = QPushButton('OPEN', parent=self.start_screen)
        self.start_screen_open_button.clicked.connect(lambda:self.top_bar.open_button_clicked(self))
        self.start_screen_open_button.setStyleSheet('QPushButton { font-size:12px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

        self.start_screen_instructions = QLabel(parent=self.start_screen)
        self.start_screen_instructions.setStyleSheet('QLabel { padding-top: 20px; padding-left: 20px; image: url("' + os.path.join(path_okp_graphics, 'start_screen_instructions.png').replace('\\', '/') + '"); qproperty-alignment: "AlignLeft";}')

        from modules import document_edit
        self.document_edit = document_edit
        self.document_edit.load(self, path_okp_graphics)

        from modules import player_controls
        self.player_controls = player_controls
        self.player_controls.load(self, path_okp_graphics)

        from modules import top_bar
        self.top_bar = top_bar
        self.top_bar.load(self, path_okp_graphics)

        self.document_edit_widget_pressed = False
        class document_edit_widget(QLabel):
            def enterEvent(widget, event):
                if not self.document_edit_widget_pressed:
                    generate_effect(self.document_edit_widget_animation, 'geometry', 500, [self.document_edit_widget.x(),self.document_edit_widget.y(),self.document_edit_widget.width(),self.document_edit_widget.height()], [self.document_edit_widget.x(),40,self.document_edit_widget.width(),40])
                    generate_effect(self.top_bar_widget_animation, 'geometry', 500, [self.top_bar_widget.x(),self.top_bar_widget.y(),self.top_bar_widget.width(),self.top_bar_widget.height()], [self.top_bar_widget.x(),-10,self.top_bar_widget.width(),70])
                    generate_effect(self.document_edit_panel_animation, 'geometry', 500, [self.document_edit_panel.x(),self.document_edit_panel.y(),self.document_edit_panel.width(),self.document_edit_panel.height()], [self.document_edit_panel.x(),-150,self.document_edit_panel.width(),230])
            def leaveEvent(widget, event):
                if not self.document_edit_widget_pressed:
                    generate_effect(self.document_edit_widget_animation, 'geometry', 500, [self.document_edit_widget.x(),self.document_edit_widget.y(),self.document_edit_widget.width(),self.document_edit_widget.height()], [self.document_edit_widget.x(),30,self.document_edit_widget.width(),40])
                    generate_effect(self.top_bar_widget_animation, 'geometry', 500, [self.top_bar_widget.x(),self.top_bar_widget.y(),self.top_bar_widget.width(),self.top_bar_widget.height()], [self.top_bar_widget.x(),0,self.top_bar_widget.width(),70])
                    generate_effect(self.document_edit_panel_animation, 'geometry', 500, [self.document_edit_panel.x(),self.document_edit_panel.y(),self.document_edit_panel.width(),self.document_edit_panel.height()], [self.document_edit_panel.x(),-160,self.document_edit_panel.width(),230])
            def mousePressEvent(widget, event):
                if self.document_edit_widget_pressed:
                    generate_effect(self.document_edit_widget_animation, 'geometry', 500, [self.document_edit_widget.x(),self.document_edit_widget.y(),self.document_edit_widget.width(),self.document_edit_widget.height()], [self.document_edit_widget.x(),30,self.document_edit_widget.width(),40])
                    generate_effect(self.top_bar_widget_animation, 'geometry', 500, [self.top_bar_widget.x(),self.top_bar_widget.y(),self.top_bar_widget.width(),self.top_bar_widget.height()], [self.top_bar_widget.x(),0,self.top_bar_widget.width(),70])
                    generate_effect(self.document_edit_panel_animation, 'geometry', 500, [self.document_edit_panel.x(),self.document_edit_panel.y(),self.document_edit_panel.width(),self.document_edit_panel.height()], [self.document_edit_panel.x(),-160,self.document_edit_panel.width(),230])
                    self.document_edit_widget_pressed = False
                else:
                    generate_effect(self.document_edit_widget_animation, 'geometry', 500, [self.document_edit_widget.x(),self.document_edit_widget.y(),self.document_edit_widget.width(),self.document_edit_widget.height()], [self.document_edit_widget.x(),170,self.document_edit_widget.width(),40])
                    generate_effect(self.top_bar_widget_animation, 'geometry', 500, [self.top_bar_widget.x(),self.top_bar_widget.y(),self.top_bar_widget.width(),self.top_bar_widget.height()], [self.top_bar_widget.x(),-60,self.top_bar_widget.width(),70])
                    generate_effect(self.document_edit_panel_animation, 'geometry', 500, [self.document_edit_panel.x(),self.document_edit_panel.y(),self.document_edit_panel.width(),self.document_edit_panel.height()], [self.document_edit_panel.x(),-20,self.document_edit_panel.width(),230])
                    self.document_edit_widget_pressed = True

        self.document_edit_widget = document_edit_widget(parent=self)
        self.document_edit_widget.setStyleSheet('QLabel { image: url("' + os.path.join(path_okp_graphics, 'document_edit.png').replace('\\', '/') + '"); qproperty-alignment: "AlignCenter";}')
        self.document_edit_widget_animation = QPropertyAnimation(self.document_edit_widget, b'geometry')
        self.document_edit_widget_animation.setEasingCurve(QEasingCurve.OutCirc)

        from modules import importer
        self.importer = importer
        self.importer.load(self, path_okp_graphics)

        self.show_importer_panel_button = QPushButton(parent=self)
        self.show_importer_panel_button.clicked.connect(lambda:show_importer_pannel_button_clicked(self))
        self.show_importer_panel_button.setCheckable(True)
        self.show_importer_panel_button.setIconSize(QSize(16,17))
        self.show_importer_panel_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'importer_icon.png')))
        self.show_importer_panel_button.setStyleSheet('    QPushButton { padding-top:1px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:checked { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
        self.show_importer_panel_button_animation = QPropertyAnimation(self.show_importer_panel_button, b'geometry')
        self.show_importer_panel_button_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.timer = QTimer(self)
        self.timer.setInterval(self.update_accuracy)
        self.timer.timeout.connect(lambda:update_things(self))

        self.setGeometry(QDesktopWidget().screenGeometry().width()*.1, QDesktopWidget().screenGeometry().height()*.1, QDesktopWidget().screenGeometry().width()*.8, QDesktopWidget().screenGeometry().height()*.8)
        self.lyrics_timeline_scroll.setVisible(False)
        self.document_edit_panel.setVisible(False)
        self.player_controls.widget_right.setVisible(False)
        self.player_controls.widget_left.setVisible(False)
        self.player_controls.widget.setVisible(False)
        self.top_bar_widget.setVisible(False)
        self.show_importer_panel_button.setVisible(False)
        self.document_edit_widget.setVisible(False)
        self.start_screen.setVisible(True)

        self.lyrics_metadata, self.lyrics_notes, self.txt_file, waveform_array = file_io.open_file(False)
        self.document_edit.update_metadata_info(self)
        self.top_bar.update_top_bar_info(self)
        self.player_controls.update_player_controls_state(self)

    def resizeEvent(self, event):
        self.lyrics_timeline_scroll.setGeometry(0,70,self.width(),self.height()-70-55)
        self.lyrics_timeline.setGeometry(0,0,self.lyrics_timeline_scroll.width(),self.lyrics_timeline_scroll.height())
        if self.music_length:
            self.lyrics_timeline.setGeometry(0,0,self.music_length*self.mediaplayer_zoom,self.lyrics_timeline_scroll.height()-20)

        zoom_widgets_size = [60,60]
        self.zoom_widgets.setGeometry(  self.lyrics_timeline_scroll.x() + self.lyrics_timeline_scroll.width() - zoom_widgets_size[0],
                                        self.lyrics_timeline_scroll.x() + (self.lyrics_timeline_scroll.height()*.5) - (zoom_widgets_size[1]*.5),
                                        zoom_widgets_size[0],
                                        zoom_widgets_size[1])

        self.zoomin_button.setGeometry(0,self.zoom_widgets.height()*.2,self.zoom_widgets.width()*.5,self.zoom_widgets.height()*.6)
        self.zoomout_button.setGeometry(self.zoom_widgets.width()*.5,self.zoom_widgets.height()*.2,self.zoom_widgets.width()*.5,self.zoom_widgets.height()*.6)
        self.viewnotesin_button.setGeometry(self.zoom_widgets.width()*.25,0,self.zoom_widgets.width()*.75,self.zoom_widgets.height()*.25)
        self.viewnotesout_button.setGeometry(self.zoom_widgets.width()*.25,self.zoom_widgets.height()*.75,self.zoom_widgets.width()*.75,self.zoom_widgets.height()*.25)

        if self.show_importer_panel_button.isChecked():
            self.show_importer_panel_button.setGeometry(self.width()-290,self.height()-40,30,30)
        else:
            self.show_importer_panel_button.setGeometry(self.width()-25,self.height()-40,30,30)

        self.start_screen.setGeometry(0,0,self.width(),self.height())
        self.start_screen_background.setGeometry(0,0,680,280)
        self.start_screen_right.setGeometry(self.start_screen_background.x()+self.start_screen_background.width(),self.start_screen_background.y(),self.start_screen.width()-(self.start_screen_background.x()+self.start_screen_background.width()),self.start_screen_background.height())
        self.start_screen_open_text.setGeometry(120,self.start_screen_background.height(),200,60)
        self.start_screen_open_button.setGeometry(self.start_screen_open_text.x(),self.start_screen_open_text.y() + self.start_screen_open_text.height(),200,60)
        self.start_screen_instructions.setGeometry(self.start_screen_open_text.x()+self.start_screen_open_text.width()+20,self.start_screen_open_text.y(),400,180)

        self.top_bar.resized(self)
        self.importer.resized(self)
        self.player_controls.resized(self)
        self.document_edit.resized(self)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            self.player_controls.play(self)

        if event.key() == Qt.Key_Slash:
            self.player_controls.play_button_selection.setChecked(not self.player_controls.play_button_selection.isChecked())

        if event.key() == Qt.Key_9:
            self.player_controls.new_back(self)

        if event.key() == Qt.Key_8:
            self.player_controls.note_above(self)

        if event.key() == Qt.Key_2:
            self.player_controls.note_below(self)

        if event.key() == Qt.Key_6:
            self.player_controls.note_ends_forward(self)

        if event.key() == Qt.Key_4:
            self.player_controls.note_starts_back(self)

        if event.key() == Qt.Key_1:
            self.player_controls.note_starts_forward(self)

        if event.key() == Qt.Key_3:
            self.player_controls.note_ends_back(self)

        if event.key() in [Qt.Key_Asterisk]:
            self.player_controls.golden_note(self)

        if event.key() in [Qt.Key_5]:
            self.player_controls.freestyle_note(self)

        if event.key() in [Qt.Key_Comma, Qt.Key_Period]:
            self.player_controls.line_break(self)

        if event.key() in [Qt.Key_Delete, Qt.Key_Backspace]:
            self.player_controls.note_remove(self)

        if event.key() == Qt.Key_Plus:
            zoomin_button_clicked(self)

        if event.key() == Qt.Key_Minus:
            zoomout_button_clicked(self)

        if event.key() == Qt.Key_Q:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'F3'
                frequency = 174.6141 #F3
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))


        #if event.key() == Qt.Key_2:
        #    frequency = 184.9972 #F#3
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_W:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'G3'
                frequency = 195.9977 #G3
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_3:
        #    frequency = 207.6523 #G#3
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_E:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'A4'
                frequency = 220.0000 #A3
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_4:
        #    frequency = 233.0819 #A#3
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_R:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'B4'
                frequency = 246.9417 #B3
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_T:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'C4'
                frequency = 261.6256 #C4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_6:
        #    frequency = 277.1826 #C#4
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_Y:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'D4'
                frequency = 293.6648 #D4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_7:
        #    frequency = 311.1270 #D#4
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_U:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'E4'
                frequency = 329.6276 #E4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_I:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'F4'
                frequency = 349.2282 #F4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_9:
        #    frequency = 369.9944 #F#4
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_O:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'G4'
                frequency = 391.9954 #G4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        #if event.key() == Qt.Key_0:
        #    frequency = 415.3047 #G#4
        #    time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
        #    sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))

        if event.key() == Qt.Key_P:
            if self.player_controls.listen_note_preview.isChecked():
                self.now_previewing_note = 'A5'
                frequency = 440.0000 #A4
                time = 0.5#self.lyrics_metadata['bpm'] / 60000.0
                sounddevice.play(numpy.array((10000 * numpy.sin(2 * numpy.pi * frequency * (numpy.arange(44100 * time) / 44100.0))), dtype=numpy.int16))


    def live_recording_note_thread_updated(self, result):
        self.recording_note = result

def update_things(self):
    if self.mediaplayer_is_playing:
        self.mediaplayer_current_position += ((self.update_accuracy*.001)/self.music_length)#(event.pos().x() / widget.width()) * len(self.music_waveform['full'])
        if self.player_controls.play_button_selection.isChecked() and self.selected_note:
            if self.mediaplayer_current_position*len(self.music_waveform['full']) > (int(self.lyrics_metadata['gap'] * ( len(self.music_waveform['full']) / (self.music_length * 1000))) + int(    ((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[0])) + int(    ((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[1]))):
                self.player_controls.stop_button.setVisible(False)
                self.player_controls.play_button.setVisible(True)
                self.player_controls.play_button_selection.setVisible(True)
                self.mediaplayer_is_playing = False
                self.mediaplayer_current_position = float((int(self.lyrics_metadata['gap'] * ( len(self.music_waveform['full']) / (self.music_length * 1000))) + int(((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[0]))) / len(self.music_waveform['full']))
    self.lyrics_timeline.update()

def edit_syllable_returnpressed(self):
    self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][3] = self.player_controls.edit_sylable.text()
    self.lyrics_timeline.update()

def zoomin_button_clicked(self):
    self.mediaplayer_zoom += 5.0
    if not self.mediaplayer_zoom in [*self.music_waveform]:
        threading.Thread(target=waveform.get_waveform_zoom(self, self.mediaplayer_zoom, self.music_length, self.music_waveform['full'])).start()
    self.lyrics_timeline.setGeometry(0,0,int(round(self.music_length*self.mediaplayer_zoom)),self.lyrics_timeline_scroll.height()-20)
    #self.lyrics_timeline.update()

def zoomout_button_clicked(self):
    self.mediaplayer_zoom -= 5.0
    if not self.mediaplayer_zoom in [*self.music_waveform]:
        threading.Thread(target=waveform.get_waveform_zoom(self, self.mediaplayer_zoom, self.music_length, self.music_waveform['full'])).start()
    self.lyrics_timeline.setGeometry(0,0,int(round(self.music_length*self.mediaplayer_zoom)),self.lyrics_timeline_scroll.height()-20)
    #self.lyrics_timeline.update()

def viewnotesin_button_clicked(self):
    if self.mediaplayer_viewnotes[0] + 1 < 29 and self.mediaplayer_viewnotes[-1] - 1 > -29:
        self.mediaplayer_viewnotes.insert(0, self.mediaplayer_viewnotes[0] + 1)
        self.mediaplayer_viewnotes.append(self.mediaplayer_viewnotes[-1] - 1)
    self.lyrics_timeline.update()

def viewnotesout_button_clicked(self):
    if len(self.mediaplayer_viewnotes) > 2:
        del self.mediaplayer_viewnotes[0]
        del self.mediaplayer_viewnotes[-1]
    self.lyrics_timeline.update()

def show_importer_pannel_button_clicked(self):
    if self.show_importer_panel_button.isChecked():
        generate_effect(self.importer.widget_animation, 'geometry', 500, [self.importer.widget.x(),self.importer.widget.y(),self.importer.widget.width(),self.importer.widget.height()], [self.width()-300,self.importer.widget.y(),self.importer.widget.width(),self.importer.widget.height()])
        generate_effect(self.show_importer_panel_button_animation, 'geometry', 500, [self.show_importer_panel_button.x(),self.show_importer_panel_button.y(),self.show_importer_panel_button.width(),self.show_importer_panel_button.height()], [self.width()-290,self.show_importer_panel_button.y(),self.show_importer_panel_button.width(),self.show_importer_panel_button.height()])
    else:
        generate_effect(self.importer.widget_animation, 'geometry', 500, [self.importer.widget.x(),self.importer.widget.y(),self.importer.widget.width(),self.importer.widget.height()], [self.width(),self.importer.widget.y(),self.importer.widget.width(),self.importer.widget.height()])
        generate_effect(self.show_importer_panel_button_animation, 'geometry', 500, [self.show_importer_panel_button.x(),self.show_importer_panel_button.y(),self.show_importer_panel_button.width(),self.show_importer_panel_button.height()], [self.width()-25,self.show_importer_panel_button.y(),self.show_importer_panel_button.width(),self.show_importer_panel_button.height()])

def generate_effect(widget, effect_type, duration, startValue, endValue):
    widget.setDuration(duration)
    if effect_type == 'geometry':
        widget.setStartValue(QRect(startValue[0],startValue[1],startValue[2],startValue[3]))
        widget.setEndValue(QRect(endValue[0],endValue[1],endValue[2],endValue[3]))
    elif effect_type == 'opacity':
        widget.setStartValue(startValue)
        widget.setEndValue(endValue)
    widget.start()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    #app.setDesktopSettingsAware(False)
    app.setStyle("plastique")
    app.setApplicationName("Open Karaoke Producer")

    if sys.platform == 'darwin':
        from PyQt5.QtGui import QFontDatabase
        font_database = QFontDatabase()
        font_database.addApplicationFont(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'Ubuntu-RI.ttf'))
        font_database.addApplicationFont(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'Ubuntu-R.ttf'))
        font_database.addApplicationFont(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'Ubuntu-B.ttf'))
        font_database.addApplicationFont(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'Ubuntu-BI.ttf'))

    app.setFont(QFont('Ubuntu', 10))
    app.main = openkaraokeproducer()
    app.main.show()

    sys.exit(app.exec_())




'''
C 	    65.41  -12: 130.81 	 0: 261.63 	12: 523.25 	24: 1046.50 	2093.00 	4186.01
C# -23: 69.30  -11: 138.59 	 1: 277.18 	13: 554.37 	25: 1108.73 	2217.46 	4434.92
D  -22: 73.42  -10: 146.83 	 2: 293.66 	14: 587.33 	26: 1174.66 	2349.32 	4698.64
D# -21: 77.78 	-9: 155.56 	 3: 311.13 	15: 622.25 	1244.51 	2489.02 	4978.03
E  -20: 82.41 	-8: 164.81 	 4: 329.63 	16: 659.26 	1318.51 	2637.02 	5274.04
F  -19: 87.31 	-7: 174.61 	 5: 349.23 	17: 698.46 	1396.91 	2793.83 	5587.65
F# -18: 92.50 	-6: 185.00 	 6: 369.99 	18: 739.99 	1479.98 	2959.96 	5919.91
G  -17: 98.00 	-5: 196.00 	 7: 392.00 	19: 783.99 	1567.98 	3135.96 	6271.93
G# -16: 103.83 	-4: 207.65 	 8: 415.30 	20: 830.61 	1661.22 	3322.44 	6644.88
A  -15: 110.00 	-3: 220.00 	 9: 440.00 	21: 880.00 	1760.00 	3520.00 	7040.00
A# -14: 116.54 	-2: 233.08 	10: 466.16 	22: 932.33 	1864.66 	3729.31 	7458.62
B  -13: 123.47 	-1: 246.94 	11: 493.88 	23: 987.77 	1975.53 	3951.07 	7902.13 '''
