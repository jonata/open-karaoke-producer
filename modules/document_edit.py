#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from PyQt5.QtWidgets import QLineEdit, QLabel, QPushButton, QCompleter, QComboBox, QSpinBox, QCheckBox
from PyQt5.QtCore import QPropertyAnimation, QEasingCurve

languages_list = ['ab - Abkhazian', 'aa - Afar', 'af - Afrikaans', 'sq - Albanian', 'am - Amharic', 'ar - Arabic', 'an - Aragonese', 'hy - Armenian', 'as - Assamese', 'ae - Avestan', 'ay - Aymara', 'az - Azerbaijani', 'ba - Bashkir', 'eu - Basque', 'be - Belarusian', 'bn - Bengali', 'bh - Bihari', 'bi - Bislama', 'bs - Bosnian', 'br - Breton', 'bg - Bulgarian', 'my - Burmese', 'ca - Catalan', 'ch - Chamorro', 'ce - Chechen', 'zh - Chinese', 'cu - Church Slavic; Slavonic; Old Bulgarian', 'cv - Chuvash', 'kw - Cornish', 'co - Corsican', 'hr - Croatian', 'cs - Czech', 'da - Danish', 'dv - Divehi; Dhivehi; Maldivian', 'nl - Dutch', 'dz - Dzongkha', 'en - English', 'eo - Esperanto', 'et - Estonian', 'fo - Faroese', 'fj - Fijian', 'fi - Finnish', 'fr - French', 'gd - Gaelic; Scottish Gaelic', 'gl - Galician', 'ka - Georgian', 'de - German', 'el - Greek, Modern (1453-)', 'gn - Guarani', 'gu - Gujarati', 'ht - Haitian; Haitian Creole', 'ha - Hausa', 'he - Hebrew', 'hz - Herero', 'hi - Hindi', 'ho - Hiri Motu', 'hu - Hungarian', 'is - Icelandic', 'io - Ido', 'id - Indonesian', 'ia - Interlingua (International Auxiliary Language Association)', 'ie - Interlingue', 'iu - Inuktitut', 'ik - Inupiaq', 'ga - Irish', 'it - Italian', 'ja - Japanese', 'jv - Javanese', 'kl - Kalaallisut', 'kn - Kannada', 'ks - Kashmiri', 'kk - Kazakh', 'km - Khmer', 'ki - Kikuyu; Gikuyu', 'rw - Kinyarwanda', 'ky - Kirghiz', 'kv - Komi', 'ko - Korean', 'kj - Kuanyama; Kwanyama', 'ku - Kurdish', 'lo - Lao', 'la - Latin', 'lv - Latvian', 'li - Limburgan; Limburger; Limburgish', 'ln - Lingala', 'lt - Lithuanian', 'lb - Luxembourgish; Letzeburgesch', 'mk - Macedonian', 'mg - Malagasy', 'ms - Malay', 'ml - Malayalam', 'mt - Maltese', 'gv - Manx', 'mi - Maori', 'mr - Marathi', 'mh - Marshallese', 'mo - Moldavian', 'mn - Mongolian', 'na - Nauru', 'nv - Navaho, Navajo', 'nd - Ndebele, North', 'nr - Ndebele, South', 'ng - Ndonga', 'ne - Nepali', 'se - Northern Sami', 'no - Norwegian', 'nb - Norwegian Bokmal', 'nn - Norwegian Nynorsk', 'ny - Nyanja; Chichewa; Chewa', 'oc - Occitan (post 1500); Provencal', 'or - Oriya', 'om - Oromo', 'os - Ossetian; Ossetic', 'pi - Pali', 'pa - Panjabi', 'fa - Persian', 'pl - Polish', 'pt - Portuguese', 'ps - Pushto', 'qu - Quechua', 'rm - Raeto-Romance', 'ro - Romanian', 'rn - Rundi', 'ru - Russian', 'sm - Samoan', 'sg - Sango', 'sa - Sanskrit', 'sc - Sardinian', 'sr - Serbian', 'sn - Shona', 'ii - Sichuan Yi', 'sd - Sindhi', 'si - Sinhala; Sinhalese', 'sk - Slovak', 'sl - Slovenian', 'so - Somali', 'st - Sotho, Southern', 'es - Spanish; Castilian', 'su - Sundanese', 'sw - Swahili', 'ss - Swati', 'sv - Swedish', 'tl - Tagalog', 'ty - Tahitian', 'tg - Tajik', 'ta - Tamil', 'tt - Tatar', 'te - Telugu', 'th - Thai', 'bo - Tibetan', 'ti - Tigrinya', 'to - Tonga (Tonga Islands)', 'ts - Tsonga', 'tn - Tswana', 'tr - Turkish', 'tk - Turkmen', 'tw - Twi', 'ug - Uighur', 'uk - Ukrainian', 'ur - Urdu', 'uz - Uzbek', 'vi - Vietnamese', 'vo - Volapuk', 'wa - Walloon', 'cy - Welsh', 'fy - Western Frisian', 'wo - Wolof', 'xh - Xhosa', 'yi - Yiddish', 'yo - Yoruba', 'za - Zhuang; Chuang', 'zu - Zulu']

def load(self, path_okp_graphics):
    self.document_edit_panel = QLabel(parent=self)
    self.document_edit_panel.setObjectName('document_edit_panel')
    self.document_edit_panel.setStyleSheet('#document_edit_panel { border-top: 0; border-right: 0; border-bottom: 10px; border-left: 0; border-image: url("' + os.path.join(path_okp_graphics, "document_edit_panel.png").replace('\\', '/') + '") 0 0 10 0 stretch stretch;}')
    self.document_edit_panel_animation = QPropertyAnimation(self.document_edit_panel, b'geometry')
    self.document_edit_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.document_edit_title_label = QLabel('TITLE', parent=self.document_edit_panel)
    self.document_edit_title_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_title = QLineEdit(parent=self.document_edit_panel)
    self.document_edit_title.editingFinished.connect(lambda:document_edit_title_changed(self))

    self.document_edit_artist_label = QLabel('ARTIST', parent=self.document_edit_panel)
    self.document_edit_artist_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_artist = QLineEdit(parent=self.document_edit_panel)
    self.document_edit_artist.editingFinished.connect(lambda:document_edit_artist_changed(self))

    self.document_edit_language_label = QLabel('LANGUAGE', parent=self.document_edit_panel)
    self.document_edit_language_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_language = QComboBox(parent=self.document_edit_panel)
    self.document_edit_language.setEditable(True)
    self.document_edit_language.setCompleter(QCompleter().setModelSorting(QCompleter.CaseInsensitivelySortedModel))
    self.document_edit_language.addItems(languages_list)
    self.document_edit_language.lineEdit().editingFinished.connect(lambda:document_edit_language_changed(self))

    self.document_edit_year_label = QLabel('YEAR', parent=self.document_edit_panel)
    self.document_edit_year_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_year = QSpinBox(parent=self.document_edit_panel)
    self.document_edit_year.setMinimum(1)
    self.document_edit_year.setMaximum(10000)
    self.document_edit_year.valueChanged.connect(lambda:document_edit_year_changed(self))

    self.document_edit_genre_label = QLabel('GENRE', parent=self.document_edit_panel)
    self.document_edit_genre_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_genre = QLineEdit(parent=self.document_edit_panel)
    self.document_edit_genre.editingFinished.connect(lambda:document_edit_genre_changed(self))

    self.document_edit_creator_label = QLabel('CREATOR', parent=self.document_edit_panel)
    self.document_edit_creator_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_creator = QLineEdit(parent=self.document_edit_panel)
    self.document_edit_creator.editingFinished.connect(lambda:document_edit_creator_changed(self))

    self.document_edit_edition_label = QLabel('EDITION', parent=self.document_edit_panel)
    self.document_edit_edition_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_edition = QLineEdit(parent=self.document_edit_panel)
    self.document_edit_edition.editingFinished.connect(lambda:document_edit_edition_changed(self))

    self.document_edit_musicfile_label = QLabel('MUSIC FILE', parent=self.document_edit_panel)
    self.document_edit_musicfile_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    class document_edit_musicfile(QLabel):
        def enterEvent(widget, event):
            self.document_edit_musicfile_button.setVisible(True)
        def leaveEvent(widget, event):
            self.document_edit_musicfile_button.setVisible(False)

    self.document_edit_musicfile = document_edit_musicfile('', parent=self.document_edit_panel)
    self.document_edit_musicfile.setStyleSheet('QLabel { color:black; font-size:12px; font-family: "Ubuntu";}')

    self.document_edit_musicfile_button = QPushButton('CHANGE', parent=self.document_edit_musicfile)
    self.document_edit_musicfile_button.clicked.connect(lambda:document_edit_musicfile_button_clicked(self))
    self.document_edit_musicfile_button.setVisible(False)
    self.document_edit_musicfile_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.document_edit_coverfile_label = QLabel('COVER FILE', parent=self.document_edit_panel)
    self.document_edit_coverfile_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    class document_edit_coverfile(QLabel):
        def enterEvent(widget, event):
            self.document_edit_coverfile_button.setVisible(True)
        def leaveEvent(widget, event):
            self.document_edit_coverfile_button.setVisible(False)

    self.document_edit_coverfile = document_edit_coverfile('', parent=self.document_edit_panel)
    self.document_edit_coverfile.setStyleSheet('QLabel { color:black; font-size:12px; font-family: "Ubuntu";}')

    self.document_edit_coverfile_button = QPushButton('CHANGE', parent=self.document_edit_coverfile)
    self.document_edit_coverfile_button.clicked.connect(lambda:document_edit_coverfile_button_clicked(self))
    self.document_edit_coverfile_button.setVisible(False)
    self.document_edit_coverfile_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.document_edit_backgroundfile_label = QLabel('BACKGROUND FILE', parent=self.document_edit_panel)
    self.document_edit_backgroundfile_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    class document_edit_backgroundfile(QLabel):
        def enterEvent(widget, event):
            self.document_edit_backgroundfile_button.setVisible(True)
        def leaveEvent(widget, event):
            self.document_edit_backgroundfile_button.setVisible(False)

    self.document_edit_backgroundfile = document_edit_backgroundfile('', parent=self.document_edit_panel)
    self.document_edit_backgroundfile.setStyleSheet('QLabel { color:black; font-size:12px; font-family: "Ubuntu";}')

    self.document_edit_backgroundfile_button = QPushButton('CHANGE', parent=self.document_edit_backgroundfile)
    self.document_edit_backgroundfile_button.clicked.connect(lambda:document_edit_backgroundfile_button_clicked(self))
    self.document_edit_backgroundfile_button.setVisible(False)
    self.document_edit_backgroundfile_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.document_edit_videofile_label = QLabel('VIDEO FILE', parent=self.document_edit_panel)
    self.document_edit_videofile_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    class document_edit_videofile(QLabel):
        def enterEvent(widget, event):
            self.document_edit_videofile_button.setVisible(True)
        def leaveEvent(widget, event):
            self.document_edit_videofile_button.setVisible(False)

    self.document_edit_videofile = document_edit_videofile('', parent=self.document_edit_panel)
    self.document_edit_videofile.setStyleSheet('QLabel { color:black; font-size:12px; font-family: "Ubuntu";}')

    self.document_edit_videofile_button = QPushButton('CHANGE', parent=self.document_edit_videofile)
    self.document_edit_videofile_button.clicked.connect(lambda:document_edit_videofile_button_clicked(self))
    self.document_edit_videofile_button.setVisible(False)
    self.document_edit_videofile_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.document_edit_videofile_gap_label = QLabel('VIDEO GAP', parent=self.document_edit_panel)
    self.document_edit_videofile_gap_label.setStyleSheet('QLabel { color:gray; font-size:9px; font-family: "Ubuntu";}')

    self.document_edit_videofile_gap = QSpinBox(parent=self.document_edit_panel)
    self.document_edit_videofile_gap.setMinimum(0)
    self.document_edit_videofile_gap.setMaximum(10000)
    self.document_edit_videofile_gap.valueChanged.connect(lambda:document_edit_videofile_gap_changed(self))

    self.document_edit_save_lyrics_in_mp3 = QCheckBox('Save lyrics in mp3/oga file', parent=self.document_edit_panel)

def resized(self):
    if self.document_edit_widget_pressed:
        self.document_edit_widget.setGeometry(self.width()-70,160,70,40)
        self.document_edit_panel.setGeometry(0,-20,self.width(),230)

    else:
        self.document_edit_widget.setGeometry(self.width()-70,30,70,40)
        self.document_edit_panel.setGeometry(0,-160,self.width(),230)

    self.document_edit_title_label.setGeometry(15,40,((self.document_edit_panel.width()-40)*.1),15)
    self.document_edit_title.setGeometry(15,55,(self.document_edit_panel.width()-40)*.5,25)

    self.document_edit_artist_label.setGeometry(((self.document_edit_panel.width()-30)*.5) + 15,40,(self.document_edit_panel.width()-40)*.5,15)
    self.document_edit_artist.setGeometry(((self.document_edit_panel.width()-30)*.5) + 15,55,(self.document_edit_panel.width()-40)*.5,25)

    self.document_edit_language_label.setGeometry(15,85,((self.document_edit_panel.width()-40)*.12)-5,15)
    self.document_edit_language.setGeometry(15,100,((self.document_edit_panel.width()-40)*.12)-5,25)
    self.document_edit_year_label.setGeometry(25+(self.document_edit_panel.width()-40)*.12,85,((self.document_edit_panel.width()-40)*.12)-5,15)
    self.document_edit_year.setGeometry(25+(self.document_edit_panel.width()-40)*.12,100,((self.document_edit_panel.width()-40)*.12)-5,25)
    self.document_edit_genre_label.setGeometry(25+(self.document_edit_panel.width()-40)*.25,85,((self.document_edit_panel.width()-40)*.25)-5,15)
    self.document_edit_genre.setGeometry(25+(self.document_edit_panel.width()-40)*.25,100,((self.document_edit_panel.width()-40)*.25)-5,25)
    self.document_edit_creator_label.setGeometry(25+(self.document_edit_panel.width()-40)*.5,85,((self.document_edit_panel.width()-40)*.25)-5,15)
    self.document_edit_creator.setGeometry(25+(self.document_edit_panel.width()-40)*.5,100,((self.document_edit_panel.width()-40)*.25)-5,25)
    self.document_edit_edition_label.setGeometry(25+(self.document_edit_panel.width()-40)*.75,85,((self.document_edit_panel.width()-40)*.25)-5,15)
    self.document_edit_edition.setGeometry(25+(self.document_edit_panel.width()-40)*.75,100,((self.document_edit_panel.width()-40)*.25)-5,25)

    self.document_edit_musicfile_label.setGeometry(15,135,(self.document_edit_panel.width()-40)*.22,15)
    self.document_edit_musicfile.setGeometry(15,150,(self.document_edit_panel.width()-40)*.22,25)
    self.document_edit_musicfile_button.setGeometry(self.document_edit_musicfile.width()-75,0,75,self.document_edit_musicfile.height())

    self.document_edit_coverfile_label.setGeometry(15+((self.document_edit_panel.width()-40)*.22)+10,135,(self.document_edit_panel.width()-40)*.22,15)
    self.document_edit_coverfile.setGeometry(15+((self.document_edit_panel.width()-40)*.22)+10,150,(self.document_edit_panel.width()-40)*.22,25)
    self.document_edit_coverfile_button.setGeometry(self.document_edit_coverfile.width()-75,0,75,self.document_edit_coverfile.height())

    self.document_edit_backgroundfile_label.setGeometry(15+((self.document_edit_panel.width()-40)*.44)+15,135,(self.document_edit_panel.width()-40)*.22,15)
    self.document_edit_backgroundfile.setGeometry(15+((self.document_edit_panel.width()-40)*.44)+15,150,(self.document_edit_panel.width()-40)*.22,25)
    self.document_edit_backgroundfile_button.setGeometry(self.document_edit_backgroundfile.width()-75,0,75,self.document_edit_backgroundfile.height())

    self.document_edit_videofile_label.setGeometry(15+((self.document_edit_panel.width()-40)*.66)+20,135,(self.document_edit_panel.width()-40)*.22,15)
    self.document_edit_videofile.setGeometry(15+((self.document_edit_panel.width()-40)*.66)+20,150,(self.document_edit_panel.width()-40)*.22,25)
    self.document_edit_videofile_button.setGeometry(self.document_edit_videofile.width()-75,0,75,self.document_edit_videofile.height())

    self.document_edit_videofile_gap_label.setGeometry(10+15+((self.document_edit_panel.width()-40)*.88)+15,135,((self.document_edit_panel.width()-40)*.1),15)
    self.document_edit_videofile_gap.setGeometry(10+15+((self.document_edit_panel.width()-40)*.88)+15,150,((self.document_edit_panel.width()-40)*.1),25)

    self.document_edit_save_lyrics_in_mp3.setGeometry(15, 185,200,20)

def document_edit_title_changed(self):
    self.lyrics_metadata['title'] = self.document_edit_title.text()
    #update_metadata_info(self)
    self.top_bar.update_top_bar_info(self)

def document_edit_artist_changed(self):
    self.lyrics_metadata['artist'] = self.document_edit_artist.text()
    #update_metadata_info(self)
    self.top_bar.update_top_bar_info(self)

def update_metadata_info(self):
    self.document_edit_title.setText(self.lyrics_metadata['title'])
    self.document_edit_artist.setText(self.lyrics_metadata['artist'])
    mp3_filepath = 'No music file selected'
    if self.lyrics_metadata['mp3']:
        mp3_filepath = self.lyrics_metadata['mp3']
    self.document_edit_musicfile.setText(mp3_filepath)

    cover_filepath = 'No cover file selected'
    if self.lyrics_metadata['cover']:
        cover_filepath = self.lyrics_metadata['cover']
    self.document_edit_coverfile.setText(cover_filepath)

    background_filepath = 'No background file selected'
    if self.lyrics_metadata['background']:
        background_filepath = self.lyrics_metadata['background']
    self.document_edit_backgroundfile.setText(background_filepath)

    video_filepath = 'No video file selected'
    if self.lyrics_metadata['video']:
        video_filepath = self.lyrics_metadata['video']
    self.document_edit_videofile.setText(video_filepath)

    self.document_edit_language.setCurrentText(self.lyrics_metadata['language'])
    self.document_edit_year.setValue(int(self.lyrics_metadata['year']))
    self.document_edit_genre.setText(self.lyrics_metadata['genre'])
    self.document_edit_creator.setText(self.lyrics_metadata['creator'])
    self.document_edit_edition.setText(self.lyrics_metadata['edition'])

def document_edit_musicfile_button_clicked(self):
    file_to_open = QFileDialog.getOpenFileName(self, "Select the music file", os.path.expanduser("~"), "MP3 file (*.mp3);;Theora Vorbis file (*.ogg, *.oga)")[0]
    if file_to_open and os.path.isfile(file_to_open):
        self.lyrics_metadata['mp3'] = file_to_open
        update_metadata_info(self)

def document_edit_coverfile_button_clicked(self):
    file_to_open = QFileDialog.getOpenFileName(self, "Select the cover file", os.path.expanduser("~"), "PNG or JPG file (*.png, *.jpg)")[0]
    if file_to_open and os.path.isfile(file_to_open):
        self.lyrics_metadata['cover'] = file_to_open
        update_metadata_info(self)

def document_edit_backgroundfile_button_clicked(self):
    file_to_open = QFileDialog.getOpenFileName(self, "Select the background file", os.path.expanduser("~"), "PNG or JPG file (*.png, *.jpg)")[0]
    if file_to_open and os.path.isfile(file_to_open):
        self.lyrics_metadata['background'] = file_to_open
        update_metadata_info(self)

def document_edit_videofile_button_clicked(self):
    file_to_open = QFileDialog.getOpenFileName(self, "Select the video file", os.path.expanduser("~"), "MP4 file (*.mp4)")[0]
    if file_to_open and os.path.isfile(file_to_open):
        self.lyrics_metadata['video'] = file_to_open
        update_metadata_info(self)

def document_edit_videofile_gap_changed(self):
    self.lyrics_metadata['videogap'] = str(self.document_edit_videofile_gap.value())

def document_edit_language_changed(self):
    self.lyrics_metadata['language'] = self.document_edit_language.currentText()

def document_edit_year_changed(self):
    self.lyrics_metadata['year'] = str(self.document_edit_year.value())

def document_edit_genre_changed(self):
    self.lyrics_metadata['genre'] = self.document_edit_genre.text()

def document_edit_creator_changed(self):
    self.lyrics_metadata['creator'] = self.document_edit_creator.text()

def document_edit_edition_changed(self):
    self.lyrics_metadata['edition'] = self.document_edit_edition.text()
