#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, threading
from PyQt5.QtWidgets import QPushButton, QLabel, QFileDialog, QSpinBox, QDoubleSpinBox
from PyQt5.QtCore import QPropertyAnimation, QEasingCurve

from modules import file_io
from modules import waveform

import sounddevice

def load(self, path_okp_graphics):
    self.top_bar_widget = QLabel(parent=self)
    self.top_bar_widget.setObjectName('top_bar')
    self.top_bar_widget.setStyleSheet('#top_bar { border-top: 0; border-right: 78px; border-bottom: 0; border-left: 0; border-image: url("' + os.path.join(path_okp_graphics, "top_bar.png").replace('\\', '/') + '") 0 78 0 0 stretch stretch;}')
    self.top_bar_widget_animation = QPropertyAnimation(self.top_bar_widget, b'geometry')
    self.top_bar_widget_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.top_bar_widget_title = QLabel(parent=self.top_bar_widget)
    self.top_bar_widget_title.setStyleSheet('QLabel { color: #E6E6E6; padding-left:15px; font-size:14px; font-family: "Ubuntu"; qproperty-alignment: "AlignVCenter";}')

    class bpm_title(QLabel):
        def mouseDoubleClickEvent(widget, event):
            self.bpm_spinbox.setVisible(True)

    self.bpm_title = bpm_title(parent=self.top_bar_widget)
    self.bpm_title.setStyleSheet('QLabel { color: silver; padding-left:15px; font-size:14px; font-family: "Ubuntu"; qproperty-alignment: "AlignVCenter";}')

    self.bpm_spinbox = QDoubleSpinBox(parent=self.top_bar_widget)
    self.bpm_spinbox.setMinimum(1)
    self.bpm_spinbox.setMaximum(10000)
    self.bpm_spinbox.setVisible(False)
    self.bpm_spinbox.setKeyboardTracking(False)
    self.bpm_spinbox.valueChanged.connect(lambda:bpm_changed(self))

    class gap_title(QLabel):
        def mouseDoubleClickEvent(widget, event):
            self.gap_spinbox.setVisible(True)

    self.gap_title = gap_title(parent=self.top_bar_widget)
    self.gap_title.setStyleSheet('QLabel { color: silver; padding-left:15px; font-size:14px; font-family: "Ubuntu"; qproperty-alignment: "AlignVCenter";}')

    self.gap_spinbox = QSpinBox(parent=self.top_bar_widget)
    self.gap_spinbox.setMinimum(0)
    self.gap_spinbox.setMaximum(1000000)
    self.gap_spinbox.setVisible(False)
    self.gap_spinbox.setKeyboardTracking(False)
    self.gap_spinbox.valueChanged.connect(lambda:gap_changed(self))

    self.save_button = QPushButton('SAVE', parent=self.top_bar_widget)
    self.save_button.clicked.connect(lambda:save_button_clicked(self))
    self.save_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.open_button = QPushButton('OPEN', parent=self.top_bar_widget)
    self.open_button.clicked.connect(lambda:open_button_clicked(self))
    self.open_button.setStyleSheet('QPushButton { font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none;  } QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

def resized(self):
    if self.document_edit_widget_pressed:
        self.top_bar_widget.setGeometry(0,-60,self.width(),70)
    else:
        self.top_bar_widget.setGeometry(0,0,self.width(),70)

    self.top_bar_widget_title.setGeometry(0,0,self.top_bar_widget.width()-80,self.top_bar_widget.height())
    self.bpm_title.setGeometry(self.top_bar_widget.width()-80-355,33,110,30)
    self.bpm_spinbox.setGeometry(self.top_bar_widget.width()-80-355,33,110,30)
    self.gap_title.setGeometry(self.top_bar_widget.width()-80-240,33,110,30)
    self.gap_spinbox.setGeometry(self.top_bar_widget.width()-80-240,33,110,30)
    self.open_button.setGeometry(self.top_bar_widget.width()-80-60,33,60,30)
    self.save_button.setGeometry(self.top_bar_widget.width()-80-125,33,60,30)

def save_button_clicked(self):
    if not self.txt_file:
        self.txt_file = QFileDialog.getSaveFileName(self, "Select the txt file", os.path.join(os.environ.get('HOME', None), self.lyrics_metadata['mp3'].rsplit('.',1)[0] + '.txt'), "TXT file (*.txt)")[0]
    if self.txt_file:
        file_io.save_file(self.lyrics_metadata, self.lyrics_notes, self.txt_file, embed_lyrics=self.document_edit_save_lyrics_in_mp3.isChecked())



def open_button_clicked(self):
    file_to_open = QFileDialog.getOpenFileName(self, "Select the txt or mp3 file", os.path.expanduser("~"), "TXT file (*.txt);;MP3 file (*.mp3);;Theora Vorbis file (*.ogg, *.oga)")[0]
    if file_to_open and os.path.isfile(file_to_open):
        self.lyrics_metadata, self.lyrics_notes, self.txt_file, waveform_array = file_io.open_file(file_to_open)

        self.music_length = self.lyrics_metadata['duration']
        self.music_waveform = {'full': waveform_array }

        threading.Thread(target=waveform.get_waveform_zoom(self, 100.0, self.music_length, self.music_waveform['full'])).start()

        self.player_controls.update_mediaplayer_viewnotes(self)

        self.lyrics_timeline.setGeometry(0,0,int(round(self.music_length*self.mediaplayer_zoom)),self.lyrics_timeline_scroll.height()-20)

        #self.lyrics_mediaplayer.set_media(self.lyrics_mediaplayer_instance.media_new((self.lyrics_metadata['mp3'])))
        sounddevice.default.samplerate = 44100
        sounddevice.default.blocksize = 2048
        #self.lyrics_mediaplayer.audio_set_volume(100)
        #self.lyrics_mediaplayer.play()
        self.lyrics_timeline_scroll.setVisible(True)
        self.document_edit_panel.setVisible(True)
        self.player_controls.widget_left.setVisible(True)
        self.player_controls.widget_right.setVisible(True)
        self.player_controls.widget.setVisible(True)
        self.top_bar_widget.setVisible(True)
        self.show_importer_panel_button.setVisible(True)
        self.document_edit_widget.setVisible(True)
        self.start_screen.setVisible(False)

        self.timer.start()
        self.document_edit.update_metadata_info(self)
        update_top_bar_info(self)
        self.player_controls.update_player_controls_state(self)

def update_top_bar_info(self):
    self.top_bar_widget_title.setText('<big>' + self.lyrics_metadata['title'] + '</big><br><small>' + self.lyrics_metadata['artist'] + '</small>')
    self.bpm_title.setText('<small>BPM</small>: ' + str(self.lyrics_metadata['bpm']))
    self.bpm_spinbox.setValue(self.lyrics_metadata['bpm'])
    self.gap_title.setText('<small>GAP</small>: ' + str(self.lyrics_metadata['gap']))
    self.gap_spinbox.setValue(self.lyrics_metadata['gap'])

def bpm_changed(self):
    self.lyrics_metadata['bpm'] = self.bpm_spinbox.value()
    threading.Thread(target=waveform.waveform_to_tones(self, self.music_waveform['full'], (44100 * 60)/self.lyrics_metadata['bpm'])).start()
    self.bpm_spinbox.setVisible(False)
    update_top_bar_info(self)

def gap_changed(self):
    self.lyrics_metadata['gap'] = self.gap_spinbox.value()
    self.gap_spinbox.setVisible(False)
    update_top_bar_info(self)
