#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, threading
from PyQt5.QtWidgets import QPushButton, QLabel, QFileDialog, QLineEdit
from PyQt5.QtCore import QPropertyAnimation, QEasingCurve, Qt, QThread, pyqtSignal, QSize
from PyQt5.QtGui import QIcon

import sounddevice, numpy, librosa

from modules import waveform

class live_recording_note(QThread):
    signal = pyqtSignal('PyQt_PyObject')
    def __init__(self):
        QThread.__init__(self)
        self.sr = 44100
        self.frame = int(self.sr/10)

    def run(self):
        def callback(indata, frames, time, status):
            fft = numpy.abs(numpy.fft.fft(indata[:, 0]))
            fft = fft[:int(len(fft)/2)]
            freq = numpy.fft.fftfreq(frames,1.0/self.sr)
            freq = freq[:int(len(freq)/2)]
            freqPeak = freq[numpy.where(fft==numpy.max(fft))[0][0]]+1
            self.signal.emit(librosa.hz_to_note(freqPeak))

        with sounddevice.InputStream(device=None, channels=1, callback=callback, blocksize=self.frame, samplerate=self.sr):
            while True:
                None

def load(self, path_okp_graphics):
    self.player_controls.live_recording_note_thread = live_recording_note()
    self.player_controls.live_recording_note_thread.signal.connect(self.live_recording_note_thread_updated)

    self.player_controls.widget = QLabel(parent=self)
    self.player_controls.widget.setObjectName('player_controls_widget')
    self.player_controls.widget.setStyleSheet('#player_controls_widget { border-top: 8px; border-right: 38px; border-bottom: 40px; border-left: 38px; border-image: url("' + os.path.join(path_okp_graphics, "player_background.png").replace('\\', '/') + '") 40 38 38 38 stretch stretch;}')

    self.player_controls.listen_note_preview = QPushButton(parent=self.player_controls.widget)
    self.player_controls.listen_note_preview.setIconSize(QSize(16,17))
    self.player_controls.listen_note_preview.setIcon(QIcon(os.path.join(path_okp_graphics, 'listen_note_preview.png')))
    self.player_controls.listen_note_preview.setCheckable(True)
    self.player_controls.listen_note_preview.setStyleSheet('    QPushButton { padding-top: 1px; font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')


    self.player_controls.widget_left = QLabel(parent=self)
    self.player_controls.widget_left.setObjectName('player_controls_widget_left')
    self.player_controls.widget_left.setStyleSheet('#player_controls_widget_left { border-top: 8px; border-right: 0; border-bottom: 40px; border-left: 0; border-image: url("' + os.path.join(path_okp_graphics, "player_background_left.png").replace('\\', '/') + '") 40 0 38 0 stretch stretch;}')

    self.player_controls.view_waveform = QPushButton(parent=self.player_controls.widget_left)
    self.player_controls.view_waveform.setIconSize(QSize(16,17))
    self.player_controls.view_waveform.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_waveform.png')))
    self.player_controls.view_waveform.clicked.connect(lambda:view_changed(self, mode='waveform'))
    self.player_controls.view_waveform.setCheckable(True)
    self.player_controls.view_waveform.setStyleSheet('    QPushButton { padding-top: 1px; font-size:10px; color:white; border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.view_verticalform = QPushButton(parent=self.player_controls.widget_left)
    self.player_controls.view_verticalform.setIconSize(QSize(16,17))
    self.player_controls.view_verticalform.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_verticalform.png')))
    self.player_controls.view_verticalform.clicked.connect(lambda:view_changed(self, mode='verticalform'))
    self.player_controls.view_verticalform.setCheckable(True)
    self.player_controls.view_verticalform.setStyleSheet('    QPushButton { padding-top: 1px; font-size:10px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.view_histogram = QPushButton(parent=self.player_controls.widget_left)
    self.player_controls.view_histogram.setIconSize(QSize(16,17))
    self.player_controls.view_histogram.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_histogram.png')))
    self.player_controls.view_histogram.clicked.connect(lambda:view_changed(self, mode='histogram'))
    self.player_controls.view_histogram.setCheckable(True)
    self.player_controls.view_histogram.setStyleSheet('    QPushButton { padding-top: 1px; font-size:10px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.view_notes = QPushButton(parent=self.player_controls.widget_left)
    self.player_controls.view_notes.setIconSize(QSize(16,17))
    self.player_controls.view_notes.setIcon(QIcon(os.path.join(path_okp_graphics, 'view_notes.png')))
    self.player_controls.view_notes.clicked.connect(lambda:view_changed(self, mode='notes'))
    self.player_controls.view_notes.setCheckable(True)
    self.player_controls.view_notes.setStyleSheet('    QPushButton { padding-top: 1px; font-size:10px; color:white; border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_2_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.widget_right = QLabel(parent=self)
    self.player_controls.widget_right.setObjectName('player_controls_widget_right')
    self.player_controls.widget_right.setStyleSheet('#player_controls_widget_right { border-top: 8px; border-right: 0; border-bottom: 40px; border-left: 0; border-image: url("' + os.path.join(path_okp_graphics, "player_background_right.png").replace('\\', '/') + '") 40 0 38 0 stretch stretch;}')

    self.player_controls.edit_sylable = QLineEdit(parent=self.player_controls.widget_right)
    self.player_controls.edit_sylable.returnPressed.connect(lambda:edit_syllable_returnpressed(self))

    self.player_controls.slice_button = QPushButton(parent=self.player_controls.widget_right)
    self.player_controls.slice_button.setIconSize(QSize(26,21))
    self.player_controls.slice_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_slice.png')))
    self.player_controls.slice_button.clicked.connect(lambda:slice_button_clicked(self))
    self.player_controls.slice_button.setStyleSheet('    QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')


    self.player_controls.line_break_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.line_break_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.line_break_button.setIconSize(QSize(20,16))
    self.player_controls.line_break_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_line_break.png')))
    self.player_controls.line_break_button.setStyleSheet('  QPushButton { padding-top: 1px; color:white; border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.note_starts_back_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_starts_back_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_starts_back_button.clicked.connect(lambda:note_starts_back(self))
    self.player_controls.note_starts_back_button.setIconSize(QSize(7,15))
    self.player_controls.note_starts_back_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_right.png')))
    self.player_controls.note_starts_back_button.setStyleSheet('    QPushButton { padding-top: 1px; color:white; border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover:pressed { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:disabled { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover { border-left: 5px; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.note_starts_forward_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_starts_forward_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_starts_forward_button.setIconSize(QSize(7,15))
    self.player_controls.note_starts_forward_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_left.png')))
    self.player_controls.note_starts_forward_button.clicked.connect(lambda:note_starts_forward(self))
    self.player_controls.note_starts_forward_button.setStyleSheet('     QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                        QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                        QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                        QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.note_above_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_above_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_above_button.clicked.connect(lambda:note_above(self))
    self.player_controls.note_above_button.setIconSize(QSize(14,8))
    self.player_controls.note_above_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_up.png')))
    self.player_controls.note_above_button.setStyleSheet('  QPushButton { padding-top: 1px; padding-bottom: 4px; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 15 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 15 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 15 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 0; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 15 5 stretch stretch; outline: none; } ')

    self.player_controls.note_below_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_below_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_below_button.clicked.connect(lambda:note_below(self))
    self.player_controls.note_below_button.setIconSize(QSize(14,8))
    self.player_controls.note_below_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_down.png')))
    self.player_controls.note_below_button.setStyleSheet('  QPushButton { padding-top: 5px; border-left: 0; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 0; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.freestyle_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.freestyle_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.freestyle_button.setCheckable(True)
    self.player_controls.freestyle_button.clicked.connect(lambda:freestyle_note(self))
    self.player_controls.freestyle_button.setIconSize(QSize(4,6))
    self.player_controls.freestyle_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_freestyle.png')))
    self.player_controls.freestyle_button.setStyleSheet('  QPushButton { padding-top: 1px; border-left: 1px; border-top: 1px; border-right: 1px; border-bottom: 1px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 1px; border-top: 1px; border-right: 1px; border-bottom: 1px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 1px; border-top: 1px; border-right: 1px; border-bottom: 1px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 1px; border-top: 1px; border-right: 1px; border-bottom: 1px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 1px; border-top: 1px; border-right: 1px; border-bottom: 1px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 15 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.play_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.play_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.play_button.clicked.connect(lambda:play(self))
    self.player_controls.play_button.setIconSize(QSize(20,21))
    self.player_controls.play_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_play.png')))
    self.player_controls.play_button.setStyleSheet('    QPushButton { padding-top: 1px; padding-right:30px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.play_button_selection = QPushButton(parent=self.player_controls.widget)
    self.player_controls.play_button_selection.setFocusPolicy(Qt.NoFocus)
    self.player_controls.play_button_selection.setCheckable(True)
    self.player_controls.play_button_selection.setIconSize(QSize(20,16))
    self.player_controls.play_button_selection.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_play_selection.png')))
    self.player_controls.play_button_selection.setStyleSheet('  QPushButton { padding-top: 1px; color:white; border-left: 2px; border-top: 2px; border-right: 0; border-bottom: 2px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:checked { border-left: 2px; border-top: 2px; border-right: 0; border-bottom: 2px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:pressed:hover { border-left: 2px; border-top: 2px; border-right: 0; border-bottom: 2px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:disabled { border-left: 2px; border-top: 2px; border-right: 0; border-bottom: 2px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:hover { border-left: 2px; border-top: 2px; border-right: 0; border-bottom: 2px; border-image: url("' + os.path.join(path_okp_graphics, 'button_1_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
    #self.player_controls.play_button_selection.setCheckable(True)

    self.player_controls.stop_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.stop_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.stop_button.setVisible(False)
    self.player_controls.stop_button.setIconSize(QSize(20,21))
    self.player_controls.stop_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_stop.png')))
    self.player_controls.stop_button.clicked.connect(lambda:stop(self))
    self.player_controls.stop_button.setStyleSheet('    QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                        QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')
    self.player_controls.stop_button.setCheckable(True)

    self.player_controls.note_ends_back_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_ends_back_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_ends_back_button.setIconSize(QSize(7,15))
    self.player_controls.note_ends_back_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_right.png')))
    self.player_controls.note_ends_back_button.clicked.connect(lambda:note_ends_back(self))
    self.player_controls.note_ends_back_button.setStyleSheet('  QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.note_ends_forward_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.note_ends_forward_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.note_ends_forward_button.setIconSize(QSize(7,15))
    self.player_controls.note_ends_forward_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_left.png')))
    self.player_controls.note_ends_forward_button.clicked.connect(lambda:note_ends_forward(self))
    self.player_controls.note_ends_forward_button.setStyleSheet('   QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                                    QPushButton:hover { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.golden_note_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.golden_note_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.golden_note_button.setCheckable(True)
    self.player_controls.golden_note_button.setIconSize(QSize(20,16))
    self.player_controls.golden_note_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_golden.png')))
    self.player_controls.golden_note_button.clicked.connect(lambda:golden_note(self))
    self.player_controls.golden_note_button.setStyleSheet(' QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:checked { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 0; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_3_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

    self.player_controls.new_note_button = QPushButton(parent=self.player_controls.widget)
    self.player_controls.new_note_button.setFocusPolicy(Qt.NoFocus)
    self.player_controls.new_note_button.setIconSize(QSize(20,16))
    self.player_controls.new_note_button.setIcon(QIcon(os.path.join(path_okp_graphics, 'player_controls_new.png')))
    self.player_controls.new_note_button.clicked.connect(lambda:new_note(self))
    self.player_controls.new_note_button.setStyleSheet('    QPushButton { padding-top: 1px; color:white; border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_normal.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover:pressed { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_pressed.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:disabled { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_disabled.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } \
                                                            QPushButton:hover { border-left: 0; border-top: 5px; border-right: 5px; border-bottom: 5px; border-image: url("' + os.path.join(path_okp_graphics, 'button_4_hover.png').replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } ')

def resized(self):
    self.player_controls.widget.setGeometry(140,self.lyrics_timeline_scroll.y() + self.lyrics_timeline_scroll.height(),400,50)
    self.player_controls.widget_left.setGeometry(0,self.player_controls.widget.y(),self.player_controls.widget.x(),self.player_controls.widget.height())
    self.player_controls.widget_right.setGeometry(self.player_controls.widget.x() + self.player_controls.widget.width(),self.player_controls.widget.y(),self.width()-(self.player_controls.widget.x() + self.player_controls.widget.width()),self.player_controls.widget.height())

    self.player_controls.view_waveform.setGeometry(10,15,30,30)
    self.player_controls.view_verticalform.setGeometry(self.player_controls.view_waveform.x() + self.player_controls.view_waveform.width(),self.player_controls.view_waveform.y(),self.player_controls.view_waveform.width(),self.player_controls.view_waveform.height())
    self.player_controls.view_histogram.setGeometry(self.player_controls.view_verticalform.x() + self.player_controls.view_verticalform.width(),self.player_controls.view_verticalform.y(),self.player_controls.view_verticalform.width(),self.player_controls.view_verticalform.height())
    self.player_controls.view_notes.setGeometry(self.player_controls.view_histogram.x() + self.player_controls.view_histogram.width(),self.player_controls.view_histogram.y(),self.player_controls.view_histogram.width(),self.player_controls.view_histogram.height())

    self.player_controls.listen_note_preview.setGeometry(0,20,20,20)

    self.player_controls.play_button.setGeometry((self.player_controls.widget.width()*.5)-50, 6,100,40)
    self.player_controls.stop_button.setGeometry(self.player_controls.play_button.x(),self.player_controls.play_button.y(),self.player_controls.play_button.width(),self.player_controls.play_button.height())
    self.player_controls.play_button_selection.setGeometry(self.player_controls.play_button.x()+64,self.player_controls.play_button.y()+7,35,26)

    self.player_controls.note_above_button.setGeometry(self.player_controls.play_button.x()-(self.player_controls.play_button.height()),self.player_controls.play_button.y(),self.player_controls.play_button.height(),self.player_controls.play_button.height()*.5)
    self.player_controls.note_below_button.setGeometry(self.player_controls.play_button.x()-(self.player_controls.play_button.height()),self.player_controls.play_button.y()+20,self.player_controls.play_button.height(),self.player_controls.play_button.height()*.5)
    self.player_controls.freestyle_button.setGeometry(self.player_controls.note_above_button.x()+(self.player_controls.note_above_button.width()*.25),self.player_controls.note_above_button.y()+(self.player_controls.note_above_button.height()*.8),self.player_controls.note_above_button.width()*.5,self.player_controls.note_above_button.height()*.4)

    self.player_controls.note_starts_back_button.setGeometry(self.player_controls.play_button.x()-(self.player_controls.play_button.height()*2),self.player_controls.play_button.y(),self.player_controls.play_button.height()*.5,self.player_controls.play_button.height())
    self.player_controls.note_starts_forward_button.setGeometry(self.player_controls.play_button.x()-(self.player_controls.play_button.height()*1.5),self.player_controls.play_button.y(),self.player_controls.play_button.height()*.5,self.player_controls.play_button.height())

    self.player_controls.note_ends_back_button.setGeometry(self.player_controls.play_button.x()+self.player_controls.play_button.width(),self.player_controls.play_button.y(),self.player_controls.play_button.height()*.5,self.player_controls.play_button.height())
    self.player_controls.note_ends_forward_button.setGeometry(self.player_controls.play_button.x()+self.player_controls.play_button.width()+(self.player_controls.play_button.height()*.5),self.player_controls.play_button.y(),self.player_controls.play_button.height()*.5,self.player_controls.play_button.height())

    self.player_controls.line_break_button.setGeometry(self.player_controls.note_starts_back_button.x()-29,self.player_controls.note_starts_back_button.y()+2,30,30)
    self.player_controls.golden_note_button.setGeometry(self.player_controls.note_ends_forward_button.x()+self.player_controls.note_ends_forward_button.width()-1,self.player_controls.note_starts_back_button.y()+2,35,30)
    self.player_controls.new_note_button.setGeometry(self.player_controls.golden_note_button.x()+self.player_controls.golden_note_button.width(),self.player_controls.note_starts_back_button.y()+2,35,30)

    self.player_controls.edit_sylable.setGeometry(0,15,self.player_controls.widget_right.width()-100,30)
    self.player_controls.slice_button.setGeometry(self.player_controls.edit_sylable.x() + self.player_controls.edit_sylable.width(),15,60,30)

def save_button_clicked(self):
    if not self.txt_file:
        QFileDialog.getSaveFileName(self, "Select the txt file", os.path.expanduser("~"), "TXT file (*.txt)")[0]
    if self.txt_file:
        file_io.save_file(self.lyrics_metadata, self.lyrics_notes, self.txt_file)

def update_player_controls_state(self):
    self.player_controls.line_break_button.setEnabled(bool(self.selected_note))
    self.player_controls.note_starts_back_button.setEnabled(bool(self.selected_note))
    self.player_controls.note_starts_forward_button.setEnabled(bool(self.selected_note))
    if self.selected_note and not self.selected_note[-1] in ['-']:
        self.player_controls.note_above_button.setEnabled(bool(self.selected_note))
        self.player_controls.note_below_button.setEnabled(bool(self.selected_note))
        self.player_controls.freestyle_button.setEnabled(bool(self.selected_note))
        if self.selected_note[-1] in ['F']:
            self.player_controls.freestyle_button.setChecked(True)
        else:
            self.player_controls.freestyle_button.setChecked(False)
        if self.selected_note[-1] in ['*']:
            self.player_controls.golden_note_button.setChecked(True)
        else:
            self.player_controls.golden_note_button.setChecked(False)
    else:
        self.player_controls.note_above_button.setEnabled(False)
        self.player_controls.note_below_button.setEnabled(False)
        self.player_controls.freestyle_button.setEnabled(False)

    self.player_controls.note_ends_back_button.setEnabled(bool(self.selected_note))
    self.player_controls.new_note_button.setEnabled(bool(self.selected_note))
    self.player_controls.golden_note_button.setEnabled(bool(self.selected_note))
    self.player_controls.note_ends_forward_button.setEnabled(bool(self.selected_note))

    self.player_controls.play_button.setEnabled(bool(self.music_waveform))
    self.player_controls.play_button_selection.setEnabled(bool(self.music_waveform))
    self.player_controls.stop_button.setEnabled(bool(self.music_waveform))

    self.player_controls.view_waveform.setEnabled(bool(self.music_waveform))
    self.player_controls.view_verticalform.setEnabled(bool(self.music_waveform))
    self.player_controls.view_histogram.setEnabled(bool(self.music_waveform))
    self.player_controls.view_notes.setEnabled(bool(self.music_waveform))

    self.player_controls.edit_sylable.setEnabled(bool(self.selected_note))
    self.player_controls.slice_button.setEnabled(bool(self.selected_note))

def play(self):
    selected_note = self.player_controls.play_button_selection.isChecked()
    if self.mediaplayer_is_playing:
        stop(self)
    else:
        if selected_note and self.selected_note and self.selected_note[-1] in [':', '*', 'F']:
            note_start = 0
            note_start += int(self.lyrics_metadata['gap'] * ( len(self.music_waveform['full']) / (self.music_length * 1000)))
            note_start += int(    ((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[0]))
            note_end = note_start
            note_end += int(    ((len(self.music_waveform['full']) / (self.music_length/60.0))    /     self.lyrics_metadata['bpm'])*.25         * int(self.selected_note[1]))
            sounddevice.play(self.music_waveform['full'][note_start:note_end])
        else:
            sounddevice.play(self.music_waveform['full'][int(self.mediaplayer_current_position*len(self.music_waveform['full'])):])
        self.player_controls.stop_button.setVisible(True)
        self.player_controls.play_button.setVisible(False)
        self.player_controls.play_button_selection.setVisible(False)
        self.mediaplayer_is_playing = True

def stop(self):
    self.player_controls.stop_button.setVisible(False)
    self.player_controls.play_button.setVisible(True)
    self.player_controls.play_button_selection.setVisible(True)
    sounddevice.stop()
    self.mediaplayer_is_playing = False

def note_above(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][2] += 1
        update_mediaplayer_viewnotes(self)
        self.lyrics_timeline.update()

def note_below(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][2] -= 1
        update_mediaplayer_viewnotes(self)
        self.lyrics_timeline.update()

def note_starts_back(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][0] -= 1
        if len(self.selected_note) > 2:
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1] += 1
        self.lyrics_timeline.update()

def note_starts_forward(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][0] += 1
        if len(self.selected_note) > 2:
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1] -= 1
        self.lyrics_timeline.update()

def note_ends_back(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1] -= 1
        self.lyrics_timeline.update()

def note_ends_forward(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1] += 1
        self.lyrics_timeline.update()

def line_break(self):
    if self.selected_note:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] = '-'
        self.lyrics_timeline.update()

def golden_note(self):
    if self.selected_note:
        if not self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] == '*':
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] = '*'
        else:
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] = ':'
        update_player_controls_state(self)
        self.lyrics_timeline.update()

def freestyle_note(self):
    if self.selected_note:
        if not self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] == 'F':
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] = 'F'
        else:
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][-1] = ':'
        update_player_controls_state(self)
        self.lyrics_timeline.update()

def note_remove(self):
    if self.selected_note:
        del self.lyrics_notes[self.lyrics_notes.index(self.selected_note)]
        update_mediaplayer_viewnotes(self)
        self.lyrics_timeline.update()

def view_changed(self, mode=False):
    if (mode == 'waveform' and self.player_controls.view_waveform.isChecked()) or (mode == 'verticalform' and self.player_controls.view_verticalform.isChecked()) or (mode == 'histogram' and self.player_controls.view_histogram.isChecked()) or (mode == 'notes' and self.player_controls.view_notes.isChecked()):
        self.mediaplayer_view_mode = mode
        if mode == 'notes' and self.player_controls.view_notes.isChecked() and not self.music_tones:
            threading.Thread(target=waveform.waveform_to_tones(self, self.music_waveform['full'], (44100 * 60)/self.lyrics_metadata['bpm'])).start()
        if mode == 'histogram' and self.player_controls.view_histogram.isChecked():
            self.player_controls.live_recording_note_thread.start()

    else:
        self.mediaplayer_view_mode = False
    if not self.mediaplayer_view_mode == 'waveform':
        self.player_controls.view_waveform.setChecked(False)
    if not self.mediaplayer_view_mode == 'verticalform':
        self.player_controls.view_verticalform.setChecked(False)
    if not self.mediaplayer_view_mode == 'histogram':
        self.player_controls.view_histogram.setChecked(False)
        if self.player_controls.live_recording_note_thread.isRunning():
            self.player_controls.live_recording_note_thread.quit()
    if not self.mediaplayer_view_mode == 'notes':
        self.player_controls.view_notes.setChecked(False)

def new_note(self):
    start = 0
    note = 10
    if len(self.lyrics_notes) > 0:
        start = self.lyrics_notes[-1][0]
        start += self.lyrics_notes[-1][1]
        if isinstance(self.lyrics_notes[-1][2], int):
            note = self.lyrics_notes[-1][2]
    self.lyrics_notes.append([start,5,note,'',':'])

def slice_button_clicked(self):
    if self.selected_note:
        if '|' in self.player_controls.edit_sylable.text():
            text_pieces = self.player_controls.edit_sylable.text().split('|')
            old_start = self.selected_note[0]
            new_duration = int(self.selected_note[1] / len(text_pieces))
            old_note = self.selected_note[2]
            del self.lyrics_notes[self.lyrics_notes.index(self.selected_note)]
            for piece in text_pieces:
                self.lyrics_notes.append([old_start,new_duration,old_note,piece,':'])
                old_start += new_duration
        else:
            position = self.player_controls.edit_sylable.cursorPosition()
            text1 = self.player_controls.edit_sylable.text()[:position]
            text2 = self.player_controls.edit_sylable.text()[position:]
            old_duration = self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1]
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1] = int(old_duration/2)
            self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][3] = text1
            self.lyrics_notes.append([self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][0] + self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1],old_duration - self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][1],self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][2],text2,':'])
        update_edit_sylable(self)

def update_edit_sylable(self):
    if self.selected_note[-1] in [':', '*', 'F']:
        self.player_controls.edit_sylable.setVisible(True)
        self.player_controls.slice_button.setVisible(True)
        self.player_controls.edit_sylable.setText(self.selected_note[3])
    elif self.selected_note[-1] in ['-']:
        self.player_controls.edit_sylable.setVisible(False)
        self.player_controls.slice_button.setVisible(False)

def edit_syllable_returnpressed(self):
    if '|' in self.player_controls.edit_sylable.text():
        slice_button_clicked(self)
    else:
        self.lyrics_notes[self.lyrics_notes.index(self.selected_note)][3] = self.player_controls.edit_sylable.text()

def update_mediaplayer_viewnotes(self):
    t_higher = 0
    t_lower = 0
    for n in self.lyrics_notes:
        if len(n) > 2:
            t = n[2]
            if isinstance(t, int):
                if t > t_higher:
                    t_higher = t
                elif t < t_lower:
                    t_lower = t

    if t_higher > 0 and t_lower < 0:
        self.mediaplayer_viewnotes = []
        while t_higher >= t_lower:
            self.mediaplayer_viewnotes.append(t_higher)
            t_higher -= 1
