#!/usr/bin/env python3

from setuptools import setup
import os

desktop_file = '[Desktop Entry]'
desktop_file += '\nEncoding=UTF-8'
desktop_file += '\nName=Open Karaoke Producer'
desktop_file += '\nExec=python3 /usr/local/share/open-karaoke-producer/open-karaoke-producer.py'
desktop_file += '\nIcon=/usr/local/share/open-karaoke-producer/graphics/open-karaoke-producer.png'
desktop_file += '\nInfo=Open Karaoke Producer'
desktop_file += '\nCategories=Application;Multimedia'
desktop_file += '\nComment=Software to produce the TXT files used on UltraStar or Performous.'
desktop_file += '\nTerminal=false'
desktop_file += '\nType=Application'
desktop_file += '\nStartupNotify=true'
open('/tmp/' + 'open-karaoke-producer.desktop', 'w').write(desktop_file)

data_files = []
for filename in os.listdir('graphics'):
    list_of_files = []
    if filename.endswith(('.png')):
        list_of_files.append('graphics/' + filename)
    data_files.append(('share/open-karaoke-producer/graphics', list_of_files))
for filename in os.listdir('resources'):
    list_of_files = []
    if filename.endswith(('.ttf')):
        list_of_files.append('resources/' + filename)
    data_files.append(('share/open-karaoke-producer/resources', list_of_files))
for filename in os.listdir('modules'):
    list_of_files = []
    if filename.endswith(('.py')):
        list_of_files.append('modules/' + filename)
    data_files.append(('share/open-karaoke-producer/modules', list_of_files))

data_files.append(('share/open-karaoke-producer', ['open-karaoke-producer.py']))
#data_files.append(('share/applications', ['snap/gui/open-karaoke-producer.desktop']))
data_files.append(('share/applications', ['/tmp/open-karaoke-producer.desktop']))

setup(name='Open Karaoke Producer',
      version='18.11',
      description='Software to produce the TXT files used on UltraStar or Performous.',
      author='Jonatã Bolzan Loss',
      author_email='jonata@jonata.org',
      url='https://openkaraokeproducer.jonata.org/',
      #packages=['opendvdproducer'],
      data_files = data_files,
      install_requires=[
                        ]
     )
