#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, numpy, subprocess, librosa
import numpy as np
import sounddevice

ffmpeg_executable = 'ffmpeg'
STARTUPINFO = None

if sys.platform == 'darwin':
    ffmpeg_executable = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg')
elif sys.platform == 'win32' or os.name == 'nt':
    if getattr(sys, 'frozen', False):
        ffmpeg_executable = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg.exe')
    else:
        ffmpeg_executable = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg.exe')

elif sys.platform == 'win32' or os.name == 'nt':
    ACTUAL_OS = 'windows'
    PATH_SUBTITLD_USER_CONFIG = os.path.join(os.getenv('LOCALAPPDATA'), 'subtitld')
    FFMPEG_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffmpeg.exe')
    FFPROBE_EXECUTABLE = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'ffprobe.exe')
    STARTUPINFO = subprocess.STARTUPINFO()
    STARTUPINFO.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    STARTUPINFO.wShowWindow = subprocess.SW_HIDE

def ffmpeg_load_audio(filename, sr=44100, mono=True, normalize=True, in_type=numpy.int16, out_type=numpy.float32):
    channels = 1 if mono else 2
    format_strings = {
        numpy.float64: 'f64le',
        numpy.float32: 'f32le',
        numpy.int16: 's16le',
        numpy.int32: 's32le',
        numpy.uint32: 'u32le'
    }
    format_string = format_strings[in_type]
    command = [
        ffmpeg_executable,
        '-i', filename,
        '-f', format_string,
        '-acodec', 'pcm_' + format_string,
        '-ar', str(sr),
        '-ac', str(channels),
        '-']
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=STARTUPINFO)
    # bytes_per_sample = numpy.dtype(in_type).itemsize
    # frame_size = bytes_per_sample * channels
    # chunk_size = frame_size * sr

    with p.stdout as stdout:
        raw = stdout.read()
        audio = numpy.fromstring(raw, dtype=in_type).astype(out_type)

    # p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=4096, startupinfo=STARTUPINFO) # creationflags=subprocess.CREATE_NO_WINDOW,
    # bytes_per_sample = numpy.dtype(in_type).itemsize
    # frame_size = bytes_per_sample * channels
    # chunk_size = frame_size * sr # read in 1-second chunks
    # raw = b''
    # with p.stdout as stdout:
    #     while True:
    #         data = stdout.read(chunk_size)
    #         if data:
    #             raw += data
    #         else:
    #             break

    if channels > 1:
        audio = audio.reshape((-1, channels)).transpose()
    if audio.size == 0:
        return audio, sr
    if issubclass(out_type, numpy.floating):
        if normalize:
            peak = numpy.abs(audio).max()
            if peak > 0:
                audio /= peak
        elif issubclass(in_type, numpy.integer):
            audio /= numpy.iinfo(in_type).max
            
    return audio

def waveform_to_tones(self, waveform_array, framesize):
    framesize = int(framesize)

    y = waveform_array
    sr=44100
    fmin=75
    fmax=1400

    onset_frames = librosa.onset.onset_detect(y=y, sr=sr)
    pitches, magnitudes = librosa.piptrack(y=y, sr=sr, fmin=fmin, fmax=fmax, hop_length=framesize)

    notes = {}
    frame = 0
    while frame < len(waveform_array) / framesize:
        index = magnitudes[:, frame].argmax()
        pitch = pitches[index, frame]
        if (pitch != 0):
            notes[frame*framesize] = librosa.hz_to_note(pitch)
        frame += 1

    '''    framesize = int(framesize)
        y = waveform_array
        sr=44100
        notes = {}
        sr = 44100
        frame = 0
        while frame < len(waveform_array):
            #def callback(indata, frames, time, status):
            fft = numpy.abs(numpy.fft.fft(waveform_array[frame:frame+framesize]))
            fft = fft[:int(len(fft)/2)]
            freq = numpy.fft.fftfreq(framesize,1.0/sr)
            freq = freq[:int(len(freq)/2)]
            freqPeak = freq[numpy.where(fft==numpy.max(fft))[0][0]]+1
            notes[int(frame)] = librosa.hz_to_note(freqPeak)
            frame += int(framesize)
            '''


    self.music_tones = notes

def generate_waveform_zoom(zoom, duration, waveform):
    #waveform = self.audio_player_waveform['full']
    positive_values = []
    negative_values = []
    parser = 0
    while parser < len(waveform):
        positive_values.append(numpy.amax(waveform[parser:parser+int(len(waveform)/(duration*zoom))]))
        negative_values.append(numpy.amin(waveform[parser:parser+int(len(waveform)/(duration*zoom))]))
        parser += int(len(waveform)/(duration*zoom))

    #print('vou calcular')
    #fft_waveform = numpy.fft.fft(waveform)
    #print(len(fft_waveform))
    #average = []
    average = False
    parser = 0

    bps = 120/60
    #chunk = int(44100/bps)
    chunk = 4096


    '''while parser < len(waveform):
        #fft = fft[:int(len(fft)/2)] # keep only first half
        #freq = np.fft.fftfreq(CHUNK,1.0/RATE)
        #freq = freq[:int(len(freq)/2)] # keep only first half
        #freqPeak = freq[np.where(fft==np.max(fft))[0][0]]+1
        #print("peak frequency: %d Hz"%freqPeak)

        signal = waveform[parser:parser+chunk]
        #signal = signal * numpy.hanning(len(signal))
        spectrum = numpy.fft.fft(signal)
        spectrum = spectrum[:int(len(spectrum)/2)]
        freq = numpy.fft.fftfreq(chunk,1.0/44100)
        freqpeak = freq[numpy.where(spectrum==numpy.max(spectrum))[0][0]]
        #freqpeak = numpy.fft.fftfreq(spectrum)
        #print(freqpeak)
        average.append(freqpeak)
        parser += int(len(waveform)/(duration*zoom))'''



    #    #average_waveform = waveform[parser:parser+int(len(waveform)/(duration*zoom))] * numpy.hanning(len(waveform[parser:parser+int(len(waveform)/(duration*zoom))]))
    #    #average_waveform = waveform[parser:parser+4096]# * numpy.hanning(len(waveform[parser:parser+int(len(waveform)/(duration*zoom))]))
    #    #fft = abs(numpy.fft.fft(average_waveform).real)
    #    #print(len(fft))
    #    #fft = fft[:int(len(fft)/2)]
    #    #freq = numpy.fft.fftfreq(4096,1.0/44100)
    #    #freq = freq[:int(len(freq)/2)]
    #    #freqpeak = freq[numpy.where(fft==numpy.max(fft))[0][0]]
    #    #average.append(freqpeak*.01)
    #    #print(freqpeak)
    #    average.append(freqpeak)
    #    #average.append(numpy.argmax(waveform[parser:parser+int(len(waveform)/(duration*zoom))])/len(waveform[parser:parser+int(len(waveform)/(duration*zoom))]))
    #    parser += int(len(waveform)/(duration*zoom))

    #return [positive_values, negative_values], average
    return positive_values, negative_values, average

    #self.project_options.markingsubtitling_panel_editor_panel_player_timeline.update()

def get_waveform_zoom(self, zoom, duration, full_waveform):
    self.music_waveform[zoom] = generate_waveform_zoom(zoom, duration, full_waveform)
    self.lyrics_timeline.update()
