#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, codecs
from mutagen.mp3 import MP3
from mutagen.oggvorbis import OggVorbis as OGG
from mutagen.id3 import USLT, SYLT

from modules import waveform

def open_file(filepath):
    txt_file = False

    lyrics_metadata = { 'title':'Unknown title',
                        'artist':'Unknown artist',
                        'mp3':False,
                        'gap':0,
                        'bpm':1.0,
                        'language':'',
                        'genre':'',
                        'year':0,
                        'creator':'',
                        'edition':'',
                        'cover':False,
                        'video':False,
                        'videogap':0,
                        'background':False,
                        'relative':False,
                        'duration':0.0
                        }
    lyrics_notes = []

    waveform_array = []

    if filepath:
        if filepath.endswith(('.txt')):
            txt_file = filepath
        elif filepath.endswith(('.mp3')):
            if os.path.isfile(filepath.rsplit('.',1)[0] + '.txt'):
                txt_file = filepath.rsplit('.',1)[0] + '.txt'
            else:
                lyrics_metadata['mp3'] = filepath

        if txt_file:
            txt_content = codecs.open(txt_file, 'r', 'utf-8').read().replace('\r', '')
        else:
            txt_content = ''

        if '#TITLE:' in txt_content:
            lyrics_metadata['title'] = txt_content.split('#TITLE:', 1)[1].split('\n')[0]
        if '#ARTIST:' in txt_content:
            lyrics_metadata['artist'] = txt_content.split('#ARTIST:', 1)[1].split('\n')[0]
        if '#LANGUAGE:' in txt_content:
            lyrics_metadata['language'] = txt_content.split('#LANGUAGE:', 1)[1].split('\n')[0]
        if '#EDITION:' in txt_content:
            lyrics_metadata['edition'] = txt_content.split('#EDITION:', 1)[1].split('\n')[0]
        if '#GENRE:' in txt_content:
            lyrics_metadata['genre'] = txt_content.split('#GENRE:', 1)[1].split('\n')[0]
        if '#YEAR:' in txt_content:
            lyrics_metadata['year'] = txt_content.split('#YEAR:', 1)[1].split('\n')[0]
        if '#CREATOR:' in txt_content:
            lyrics_metadata['creator'] = txt_content.split('#CREATOR:', 1)[1].split('\n')[0]
        if '#MP3:' in txt_content:
            lyrics_metadata['mp3'] = os.path.join(os.path.dirname(filepath), txt_content.split('#MP3:', 1)[1].split('\n')[0])
        if '#VIDEO:' in txt_content:
            lyrics_metadata['video'] = os.path.join(txt_content.split('#VIDEO:', 1)[1].split('\n')[0])
        if '#VIDEOGAP:' in txt_content:
            lyrics_metadata['videogap'] = int(txt_content.split('#VIDEOGAP:', 1)[1].split('\n')[0])
        if '#BACKGROUND:' in txt_content:
            lyrics_metadata['background'] = os.path.join(txt_content.split('#BACKGROUND:', 1)[1].split('\n')[0])
        if '#COVER:' in txt_content:
            lyrics_metadata['cover'] = os.path.join(txt_content.split('#COVER:', 1)[1].split('\n')[0])
        if '#BPM:' in txt_content:
            lyrics_metadata['bpm'] = float(txt_content.split('#BPM:', 1)[1].split('\n')[0].replace(',','.'))
        if '#GAP:' in txt_content:
            lyrics_metadata['gap'] = int(txt_content.split('#GAP:', 1)[1].split('\n')[0])

        if not os.path.isfile(lyrics_metadata['mp3']):
            if os.path.isfile(os.path.join(os.path.dirname(filepath), os.path.basename(lyrics_metadata['mp3']))):
                lyrics_metadata['mp3'] = os.path.join(os.path.dirname(filepath), os.path.basename(lyrics_metadata['mp3']))

        if os.path.isfile(lyrics_metadata['mp3']):
            if lyrics_metadata['mp3'].endswith('.mp3'):
                media_file = MP3(lyrics_metadata['mp3'])
            elif lyrics_metadata['mp3'].endswith('.ogg'):
                media_file = OGG(lyrics_metadata['mp3'])
            if lyrics_metadata['title'] == 'Unknown title' and media_file.tags and 'TIT2' in media_file.tags.keys():
                lyrics_metadata['title'] = str(media_file.tags['TIT2']).split('/')[0]
            if lyrics_metadata['artist'] == 'Unknown artist' and media_file.tags and 'TPE1' in media_file.tags.keys():
                lyrics_metadata['artist'] = str(media_file.tags['TPE1']).split('/')[0]
            if not lyrics_metadata['edition'] and media_file.tags and 'TALB' in media_file.tags.keys():
                lyrics_metadata['edition'] = str(media_file.tags['TALB']).split('/')[0]
            if not lyrics_metadata['genre'] and media_file.tags and 'TCON' in media_file.tags.keys():
                lyrics_metadata['genre'] = str(media_file.tags['TCON']).split('/')[0]
            if not lyrics_metadata['year'] and media_file.tags and 'TDRC' in media_file.tags.keys():
                lyrics_metadata['year'] = str(media_file.tags['TDRC']).split('/')[0]
            if not lyrics_metadata['cover'] and media_file.tags and 'APIC:' in media_file.tags.keys():

                if media_file.tags['APIC:'].mime == 'image/jpeg':
                    extension = '.jpg'
                elif media_file.tags['APIC:'].mime == 'image/png':
                    extension = '.png'
                open(os.path.join(os.path.dirname(filepath), 'cover' + extension), 'wb').write(media_file.tags['APIC:'].data)
                lyrics_metadata['cover'] = os.path.join(os.path.dirname(filepath), 'cover' + extension)

            lyrics_metadata['duration'] = media_file.info.length

            waveform_array = waveform.ffmpeg_load_audio(lyrics_metadata['mp3'])

        if lyrics_metadata['bpm'] == 1:
            import librosa
            print(type(waveform_array))
            tempo, beat_frames = librosa.beat.beat_track(y=waveform_array, sr=44100)
            lyrics_metadata['bpm'] = tempo

        for line in txt_content.split('\n'):
            if line.startswith('#'):
                pass
            elif line.startswith((':','*','F','-')):
                syllable = []
                syllable.append(int(line.split(' ')[1])) # start of syllable

                kind = line.split(' ')[0] # kind of sylable

                if kind in [':', '*', 'F', '-']:
                    if kind in ['-']:
                        length = 1
                        if len(line.split(' ')) > 2:
                            length = int(line.split(' ')[2]) - int(line.split(' ')[1])
                        syllable.append(length)
                    elif kind in [':', '*', 'F']:
                        syllable.append(int(line.split(' ')[2])) # duration of syllable
                        syllable.append(int(line.split(' ')[3]))  # note of sylable
                        text = ''
                        if len(line.split(' ')) > 4:
                            text = line.split(' ',4)[4]
                        syllable.append(text)  # text of syllable

                    syllable.append(kind)

                    lyrics_notes.append(syllable)

    return lyrics_metadata, lyrics_notes, txt_file, waveform_array

def save_file(lyrics_metadata, lyrics_notes, txt_file, embed_lyrics=False):
    final_txt = ''

    if lyrics_metadata['title']:
        final_txt += '#TITLE:' + str(lyrics_metadata['title']) + '\n'

    if lyrics_metadata['artist']:
        final_txt += '#ARTIST:' + str(lyrics_metadata['artist']) + '\n'

    if lyrics_metadata['language']:
        final_txt += '#LANGUAGE:' + str(lyrics_metadata['language']) + '\n'

    if lyrics_metadata['edition']:
        final_txt += '#EDITION:' + str(lyrics_metadata['edition']) + '\n'

    if lyrics_metadata['genre']:
        final_txt += '#GENRE:' + str(lyrics_metadata['genre']) + '\n'

    if lyrics_metadata['year']:
        final_txt += '#YEAR:' + str(lyrics_metadata['year']) + '\n'

    if lyrics_metadata['creator']:
        final_txt += '#CREATOR:' + str(lyrics_metadata['creator']) + '\n'

    if lyrics_metadata['mp3']:
        final_txt += '#MP3:' + str(os.path.basename(lyrics_metadata['mp3'])) + '\n'

    if lyrics_metadata['video']:
        final_txt += '#VIDEO:' + str(os.path.basename(lyrics_metadata['video'])) + '\n'

    if lyrics_metadata['videogap']:
        final_txt += '#VIDEOGAP:' + str(lyrics_metadata['videogap']) + '\n'

    if lyrics_metadata['background']:
        final_txt += '#BACKGROUND:' + str(os.path.basename(lyrics_metadata['background'])) + '\n'

    if lyrics_metadata['cover']:
        final_txt += '#COVER:' + str(os.path.basename(lyrics_metadata['cover'])) + '\n'

    if lyrics_metadata['bpm']:
        final_txt += '#BPM:' + str(float(lyrics_metadata['bpm'])) + '\n'

    if lyrics_metadata['gap']:
        final_txt += '#GAP:' + str(int(lyrics_metadata['gap'])) + '\n'

    for syllable in sorted(lyrics_notes, key=lambda syllable: syllable[0]):
        final_txt += syllable[-1] + ' ' + str(syllable[0])
        if syllable[-1] in ['-'] and not (syllable[1] == 1):
            if not syllable[1] in ['-']:
                final_txt += ' ' + str(syllable[1] + syllable[0])
        elif syllable[-1] in [':', '*', 'F']:
            final_txt += ' ' + str(syllable[1]) + ' ' + str(syllable[2]) + ' ' + syllable[3]
        final_txt += '\n'

    final_txt += 'E'

    if embed_lyrics:
        if lyrics_metadata['mp3'].endswith('.mp3'):
            media_file = MP3(lyrics_metadata['mp3'])
        elif lyrics_metadata['mp3'].endswith('.ogg'):
            media_file = OGG(lyrics_metadata['mp3'])

        ms_per_bpm = 1000/(lyrics_metadata['bpm']/60)

        lyrics = ''
        sync_lyrics = []

        for syllable in sorted(lyrics_notes, key=lambda syllable: syllable[0]):
            if syllable[-1] in ['-']:
                lyrics += '\n'
            else:
                lyrics += syllable[3]
                sync_lyrics.append( (( syllable[3], int(lyrics_metadata['gap'] + (syllable[0]*ms_per_bpm)) )) )

        default_lang = 'eng'
        if lyrics_metadata['language']:
            default_lang = lyrics_metadata['language'].split(' - ',1)[0]

        if len(media_file.tags.getall(u"USLT:Open Karaoke Producer:'" + default_lang + "'")) != 0:
            media_file.tags.delall(u"USLT:Open Karaoke Producer:'" + default_lang + "'")
            media_file.tags.save(lyrics_metadata['mp3'])
        if len(media_file.tags.getall(u"SYLT:Open Karaoke Producer:'" + default_lang + "'")) != 0:
            media_file.tags.delall(u"SYLT:Open Karaoke Producer:'" + default_lang + "'")
            media_file.tags.save(lyrics_metadata['mp3'])

        media_file.tags[u"USLT:Open Karaoke Producer:'" + default_lang + "'"] = USLT(encoding=3, lang=default_lang, desc=u'Open Karaoke Producer', text=lyrics)
        media_file.tags[u"SYLT:Open Karaoke Producer:'" + default_lang + "'"] = SYLT(encoding=3, lang=default_lang, desc=u'Open Karaoke Producer', format=2, type=1, text=sync_lyrics)

        media_file.tags.save(lyrics_metadata['mp3'])

    codecs.open(txt_file, 'w', 'utf-8').write(final_txt)
